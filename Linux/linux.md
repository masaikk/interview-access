# Linux基础知识

在这里记录知识。

## Linux基本命令

`fg`

对于一段已经使用ctrl+z在后台运行的任务来说，可以使用这条命令将它挂到前台。

`last`

可以通过这个命令查看历史登陆记录。

`nmap`

扫描端口是否开启。

`du -h –max-depth=0 *`

查看当前目录下文件夹的大小。

`sudo useradd name`以及`sudo passwd name`创建用户以及设置密码。

`useradd -m`创建根目录。

`usermod --shell /bin/bash 名字`用于设置用户默认sh软件为bash。

---

## 基本流程

### ssh免密登录

参考[https://blog.csdn.net/jeikerxiao/article/details/84105529](https://blog.csdn.net/jeikerxiao/article/details/84105529)

首先需要在本机生成自己的ssh公钥和私钥。

```shell
ssh-keygen
```

查看文件`cd ~/.ssh`再`ls`

下创建两个密钥：

1. id_rsa （私钥）
2. id_rsa.pub (公钥)

最后上传到服务器

```shell
ssh-copy-id -i ~/.ssh/id_rsa.pub 用户@ip
```

即可以在服务器端的~/.ssh/authorized_keys看到上传的公钥，此时已经可以登录`ssh user@ip`。

如果一个服务器有多个登录公钥存在的话，在该文件上换行可以存放多个。

同时，可以指定登录的别名，就不需要每次都输入用户名的ip。在~/.ssh下创建config文件

```shell
Host 别名
  HostName Ip
  User 名字
```

之后如果放置了公钥，就可以直接使用`ssh 别名`来登录。

---

### 加装硬盘

首先将sata线供电线将硬盘插紧。[linux怎么增加硬盘-linux运维-PHP中文网](https://www.php.cn/linux-488481.html#:~:text=linux增加硬盘的方法：1、利用fdisk命令显示当前硬盘分区情况，按“n”键添加新的分区；2、利用mkfs命令格式化分区；3、利用mount命令挂载分区；4、利用“defaults 0,0”语句设置永久启动时自动挂载。 本教程操作环境：linux7.3系统、Dell G3电脑。)

使用`fdisk -l` 查看硬盘，之后分区，格式化到ext4格式。可以使用mkfs指令`mkfs.ext4 /名字`。

挂载硬盘，使用mount命令。`mount /名字 /想挂载的路径`

在挂载之后，可以查看具体的挂载情况`df -Ph /想挂载的路径`

由于linux在重启之后会清除挂载，使用需要在/etc/fstab中添加。

`/名字 /想挂载的路径 ext4 defaults 0 0`
