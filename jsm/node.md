# node

这里是node底层的学习笔记，包括v8引擎以及node服务端的知识。内容参考一个掘金小册[深入剖析 Node.js 底层原理 - theanarkh - 掘金小册 (juejin.cn)](https://juejin.cn/book/7171733571638738952)

## v8引擎

<img src="node.assets/v2-4dce785ff4595de55623611ab0055d33_1440w.jpg" alt="认识 V8 引擎" style="zoom:33%;" />

### v8引擎原理

参考[认识 V8 引擎 - 知乎 (zhihu.com)](https://zhuanlan.zhihu.com/p/27628685)

![img](node.assets/aHR0cHM6Ly91c2VyLWdvbGQtY2RuLnhpdHUuaW8vMjAyMC80LzI1LzE3MWIwNzkzMGNiNzViNTc)

webkit调用v8的结构

![preview](node.assets/v2-ad0a86d3faf223164a9bd22658feadc3_r.jpg)

首先是网页内容，输入到HTML解析器，HTML解析器解析，然后构建DOM树，在这期间如果遇到JavaScript代码则交给JavaScript引擎处理；如果来自CSS解析器的样式信息，构建一个内部绘图模型。该模型由布局模块计算模型内部各个元素的位置和大小信息，最后由绘图模块完成从该模型到图像的绘制。

而具体在v8引擎中：

源代码-→抽象语法树-→字节码-→JIT-→本地代码(V8引擎没有中间字节码)。

*在运行JavaScript之前，相比其它的JavaScript的引擎转换成字节码或解释执行，V8将其编译成原生机器码（IA-32, x86-64, ARM, or MIPS CPUs），并且使用了如内联缓存（inline caching）等方法来提高性能。*

---

### v8的内存回收

---

### v8的promise

参考这篇文章[从Google V8引擎剖析Promise实现 (fly63.com)](https://www.fly63.com/article/detial/3409)

## Nodejs底层

### 异步IO

代码里面读取某个文件，然后就调用 libuv 去读取，假设没有更多其他事件，那么堪堪调用之后，epoll 就会等着读取完毕的事件，底层代码就阻塞在等待事件上，直到读取完成，有事件通知，才会最终调用。

![image.png](node.assets/4665dd6450684797b6454e0dd43aa58ftplv-k3u1fbpfcp-jj-mark1890000q75.awebp)

比如我要在一个新线程做一件事情，又想在新线程做完事情之后，能回归到主事件循环做之后的事，这时就需要用到 `uv_async_t` 了。它的作用就是允许你在一条非 libuv 事件循环的线程中通知 libuv 说我某个事情做完了。



## NestJS

参考掘金小册[NestJS 项目实战 - CookieBoty - 掘金小册 (juejin.cn)](https://juejin.cn/book/7065201654273933316)使用`nest new mynest`创建一个叫做mynest的项目，在此之前需要安装cli工具`npm install -g @nestjs/cli`。我的代码位于[mynest: nestjs的demo (gitee.com)](https://gitee.com/masaikk/mynest)。运行之后打开http://127.0.0.1:3000/可以看到：

![image-20221101164213248](node.assets/image-20221101164213248.png)

对于这个框架来说，它包含AOP机制。Middleware、Guard、Pipe、Interceptor、ExceptionFilter 都可以透明的添加某种处理逻辑到某个路由或者全部路由，这就是 AOP 的好处。

![img](node.assets/1a6c3d7eebcc4d248bd8df8c1f71f7edtplv-k3u1fbpfcp-zoom-in-crop-mark3024000.awebp)

Nest 的 Middleware、Guard、Interceptor、Pipe、ExceptionFilter 都是 AOP 思想的实现，只不过是不同位置的切面，它们都可以灵活的作用在某个路由或者全部路由，这就是 AOP 的优势。

我们通过源码来看了它们的调用顺序，Middleware 是 Express 的概念，在最外层，到了某个路由之后，会先调用  Guard，Guard 用于判断路由有没有权限访问，然后会调用 Interceptor，对 Contoller 前后扩展一些逻辑，在到达目标  Controller 之前，还会调用 Pipe 来对参数做检验和转换。所有的 HttpException 的异常都会被  ExceptionFilter 处理，返回不同的响应。

Nest 就是通过这种 AOP 的架构方式，实现了松耦合、易于维护和扩展的架构。

### 建立controller

使用`nest g co u1`建立一个名叫u1的controller，并且这个命令会自动在app.module.ts进行注册。

```typescript
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { U1Controller } from './u1/u1.controller';

@Module({
  imports: [],
  controllers: [AppController, U1Controller],
  providers: [AppService],
})
export class AppModule {}

```

如果不需要生成spec文件，就需要在nest-cli.json中添加属性，参考

```json
{
  "$schema": "https://json.schemastore.org/nest-cli",
  "collection": "@nestjs/schematics",
  "sourceRoot": "src",
  "generateOptions": {
    "spec": false
  }
}

```

在对应u1的controller中修改到如下内容

```typescript
import { Controller, Get } from "@nestjs/common";

@Controller("u1")
export class U1Controller {
  @Get()
  getHello(): string {
    return "hello u1";
  }
}

```

浏览器中打开[127.0.0.1:3000/u1](http://127.0.0.1:3000/u1)，可以get到字符串：

![image-20221101182231234](node.assets/image-20221101182231234.png)

也可以添加Get装饰器，添加一个新的路由

```typescript
import { Controller, Get } from "@nestjs/common";

@Controller("u1")
export class U1Controller {
  @Get()
  getHello(): string {
    return "hello u1";
  }

  @Get("another")
  getAnother(): string {
    return "another greet";
  }
}

```

![image-20221120191040655](node.assets/image-20221120191040655.png)

或者直接使用`@Body()`装饰器来获取body。

如果使用RESTful的动态路由，也可以使用`@Param`装饰器来获取值，类似与官方文档中的：

```typescript
  @Get(":id")
  findOne(@Param("id") id: string) {
    return this.userService.findOne(+id);
  }
```



### 建立CURD

可以使用如下命令创建一个CURD：`nest g resource user `，选择RESTful类型的，这个情况是使用请求的方式来判断具体的操作的，打开[127.0.0.1:3000/user](http://127.0.0.1:3000/user)，对应到这里

```typescript
@Get()
  findAll() {
    return this.userService.findAll();
  }
```

展示：

![image-20221101182936970](node.assets/image-20221101182936970.png)

需要注意的是，创建单独的controller以及CRUD应用，在app.module里面的注册是不同的，参考以上的两个的注册为一个在imports中一个在controllers中。

```typescript
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { U1Controller } from './u1/u1.controller';
import { UserModule } from './user/user.module';

@Module({
  imports: [UserModule],
  controllers: [AppController, U1Controller],
  providers: [AppService],
})
export class AppModule {}
```

对于一个controller来说，可以在参数中使用@Request或者@Query装饰器来获取request或者query，例如

```typescript
  @Get()
  @Version("3")
  fundAll3(@Query() query) {
    console.log(query);
    return {
      code: 200,
      message: query.name,
    };
  }
```

![image-20221121000311929](node.assets/image-20221121000311929.png)

可以通过query对象来获取到message的值。注意它们是针对get请求来说的。

针对于post请求，可以使用requset.body的形式。类似于：

```typescript
  @Post()
  @Version("2")
  create2(@Request() req) {
    console.log(req.body);
    return {
      code: 200,
    };
  }
```

![image-20221121001232713](node.assets/image-20221121001232713.png)

### 创建Api版本号

对于一个api来说，它的内部实现以及返回可能会变，所以可以添加版本号来标识api的不同。比如一开始的[127.0.0.1:3000/user](http://127.0.0.1:3000/user)可以变为[127.0.0.1:3000/v1/user](http://127.0.0.1:3000/v1/user)。

首先在main.ts中导入版本库

```typescript
import { VersioningType } from "@nestjs/common";
import { NestFactory } from "@nestjs/core";
import { AppModule } from "./app.module";

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.enableVersioning({
    type: VersioningType.URI,
  });

  await app.listen(3000);
}

bootstrap();

```

然后对于controller的方法，添加version装饰器

```typescript
  @Get()
  @Version("1")
  findAll() {
    return this.userService.findAll();
  }
```

使用[127.0.0.1:3000/v1/user](http://127.0.0.1:3000/v1/user)访问得到：

![image-20221110201209800](node.assets/image-20221110201209800.png)

如果想创建更多不同version的api，需要修改mian.ts的配置，比如说添加v1以及v2版本的api如下：

```typescript
import { VERSION_NEUTRAL, VersioningType } from "@nestjs/common";
import { NestFactory } from "@nestjs/core";
import { AppModule } from "./app.module";

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.enableVersioning({
    type: VersioningType.URI,
    defaultVersion: [VERSION_NEUTRAL, "1", "2"],
  });

  await app.listen(3000);
}

bootstrap();

```

添加controller的第二个findAll方法

```typescript
  @Get()
  @Version("2")
  findAll2() {
    return "version 2 find all";
  }
```

使用[127.0.0.1:3000/v2/user](http://127.0.0.1:3000/v2/user)获取：

![image-20221110203948386](node.assets/image-20221110203948386.png)

### cors

[CORS | NestJS - A progressive Node.js framework](https://docs.nestjs.com/security/cors)

对于app调用开启cors的函数即可。

```typescript
const app = await NestFactory.create(AppModule);
app.enableCors();
await app.listen(3000);
```

### 守卫

守卫有一个单独的责任。它们根据运行时出现的某些条件（例如权限，角色，访问控制列表等）来确定给定的请求是否由路由处理程序处理。这通常称为授权。在传统的 Express 应用程序中，通常由中间件处理授权(以及认证)。中间件是身份验证的良好选择，因为诸如 token 验证或添加属性到 request 对象上与特定路由(及其元数据)没有强关联。

*守卫在每个中间件之后执行，但在任何拦截器或管道之前执行。*

[小满nestjs（第二十一章 nestjs 守卫）_小满zs的博客-CSDN博客_nestjs 守卫](https://blog.csdn.net/qq1195566313/article/details/127175529?ops_request_misc=%7B%22request%5Fid%22%3A%22166916857616782425124866%22%2C%22scm%22%3A%2220140713.130102334.pc%5Fall.%22%7D&request_id=166916857616782425124866&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~first_rank_ecpm_v1~rank_v31_ecpm-1-127175529-null-null.142^v66^pc_rank_34_queryrelevant25,201^v3^add_ask,213^v2^t3_esquery_v1&utm_term=小满nestjs 守卫&spm=1018.2226.3001.4187)

### RxJs

在nestjs中使用rxjs来处理异步队列，它的设计也有着“观察者模式”

```typescript
import { Observable } from "rxjs";

const observable = new Observable<any>((subscriber) => {
  subscriber.next(1);
  subscriber.next(2);
  subscriber.next(3);
  setTimeout(() => {
    subscriber.next(4);
    subscriber.complete();
  }, 3000);
});

observable.subscribe({
  next: (num) => {
    console.log(num);
  },
});
```

如上所示，定义可观测对象observable，它的参数订阅者`subscriber`可以通过`next()`来获取到消息，如上所示，对订阅者发送了四条消息。再对其进行订阅，并且打印这些消息。

![image-20221121161204131](node.assets/image-20221121161204131.png)

类似与如下代码，可以输出序列

```typescript
import { Observable, interval, take } from "rxjs";

interval(500)
  .pipe(take(5))
  .subscribe((e) => {
    console.log(e);
  });

```

这里`interval(500)`表示为每500毫秒开始从0开始输出一个数字，`take(5)`表示截断到5，所以输出为

![image-20221121161637307](node.assets/image-20221121161637307.png)



如果将take换成map，就可以进行更多的操作`map((n) => ({ num: n }))`把n转换成对象。但是没有take就不会停。

```typescript
import { Observable, interval, take } from "rxjs";
import { map } from "rxjs/operators";

interval(500)
  .pipe(map((n) => ({ num: n })))
  .subscribe((e) => {
    console.log(e);
  });
```

![image-20221121162329833](node.assets/image-20221121162329833.png)

同时也可以设计条件暂停订阅

```typescript
import { Observable, interval, take } from "rxjs";
import { map } from "rxjs/operators";

const subs = interval(500)
  .pipe(map((n) => ({ num: n })))
  .subscribe((e) => {
    console.log(e);
    if (e.num == 10) {
      subs.unsubscribe();
    }
  });

```

![image-20221121162655558](node.assets/image-20221121162655558.png)

对于传入的队列，也可以使用of来指定

```typescript
import { of, Observable, interval, take } from "rxjs";
import { map } from "rxjs/operators";

const subs = of(1, 1, 4, 5, 1, 4)
  .pipe(map((n) => ({ num: n })))
  .subscribe((e) => {
    console.log(e);
    if (e.num == 10) {
      subs.unsubscribe();
    }
  });

```

![image-20221121163315203](node.assets/image-20221121163315203.png)

### 拦截器

#### 响应拦截器

对于响应来说，需要规范响应返回的数据，例如如下类型

```typescript
{
          data,
          status: 200,
          message: "",
          success: true,
        };
```

因此，可以建立如下的响应拦截器

```typescript
import {
  Injectable,
  NestInterceptor,
  CallHandler,
  ExecutionContext,
} from "@nestjs/common";
import { map } from "rxjs/operators";
import { Observable } from "rxjs";

interface Data<T> {
  data: T;
}

@Injectable()
export class Response<T> implements NestInterceptor {
  intercept(
    context: ExecutionContext,
    next: CallHandler<any>,
  ): Observable<Data<T>> {
    return next.handle().pipe(
      map((data) => {
        return {
          data,
          status: 200,
          message: "这是一个返回，data字符串长度" + data.length,
          success: true,
        };
      }),
    );
  }
}

```

这里定义为了有类型提示，加入了接口Data，结合上一节中介绍了RxJs的导管的特点。注意加上装饰器`@Injectable()`，即可在main.ts中注入。

```typescript
import { VERSION_NEUTRAL, VersioningType } from "@nestjs/common";
import { NestFactory } from "@nestjs/core";
import { AppModule } from "./app.module";
import { Response } from "./common/response";

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.enableVersioning({
    type: VersioningType.URI,
    defaultVersion: [VERSION_NEUTRAL, "1", "2", "3"],
  });
  app.useGlobalInterceptors(new Response());

  await app.listen(3000);
}

bootstrap();

```

之后的响应都被拦截器拦截下来并且都是返回希望的格式了，例如http://127.0.0.1:3000/v1/user得到

![image-20221121211448049](node.assets/image-20221121211448049.png)

#### 请求拦截器

参考[第十七章（nestjs 异常拦截器）_哔哩哔哩_bilibili](https://www.bilibili.com/video/BV1NG41187Bs?p=17&vd_source=36542d6c49bf487d8a18d22be404b8d2)

### swagger

如果想使用swagger接口文档，需要安装依赖`npm install @nestjs/swagger swagger-ui-express -S`

首先在main.ts导入`import { SwaggerModule, DocumentBuilder } from "@nestjs/swagger";`，再创建配置

```typescript
const options = new DocumentBuilder()
  .setTitle("MyNest 文档")
  .setVersion("1")
  .build();
```

再生成文档和挂载

```typescript
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup("/api-docs", app, document);
```

最终main.ts的代码如下

```typescript
import { VERSION_NEUTRAL, VersioningType } from "@nestjs/common";
import { NestFactory } from "@nestjs/core";
import { AppModule } from "./app.module";
import { SwaggerModule, DocumentBuilder } from "@nestjs/swagger";

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.enableVersioning({
    type: VersioningType.URI,
    defaultVersion: [VERSION_NEUTRAL, "1", "2", "3"],
  });
  const options = new DocumentBuilder()
    .setTitle("MyNest 文档")
    .setVersion("1")
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup("/api-docs", app, document);

  await app.listen(3000);
}

bootstrap();

```

可以访问[Swagger UI](http://localhost:3000/api-docs#/)得到。

![image-20221121213000882](node.assets/image-20221121213000882.png)

配置具体情况参考[第二十三章（nestjs swagger接口文档）_哔哩哔哩_bilibili](https://www.bilibili.com/video/BV1NG41187Bs?p=23&vd_source=36542d6c49bf487d8a18d22be404b8d2)

### typeORM

使用typeORM连接数据库，安装依赖`npm install --save @nestjs/typeorm typeorm mysql2`

在app.module的imports中导入配置

```typescript
TypeOrmModule.forRoot({
      type: "mysql", //数据库类型
      username: "", //账号
      password: "", //密码
      host: "", //host
      port: 3306, //
      database: "nest", //库名
      // entities: [__dirname + "/**/*.entity{.ts,.js}"], //实体文件
      synchronize: true, //synchronize字段代表是否自动将实体类同步到数据库
      retryDelay: 500, //重试连接数据库间隔
      retryAttempts: 10, //重试连接数据库的次数
      autoLoadEntities: true, //如果为true,将自动加载实体 forFeature()方法注册的每个实体都将自动添加到配置对象的实体数组中
    }),
```

*需要注意的是，在生产环境，需要将`synchronize`设置为false，否则会丢失数据。*

关于实体，需要写在各resource的entities里面。在定义一个实体类的时候，需要用到typeorm的装饰器，参考[SQL (TypeORM) | NestJS - A progressive Node.js framework](https://docs.nestjs.com/recipes/sql-typeorm)以及[装饰器参考 | TypeORM 中文文档 | TypeORM 中文网 (bootcss.com)](https://typeorm.bootcss.com/decorator-reference)。例如：

```typescript
import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Orm {
  @PrimaryGeneratedColumn("uuid")
  id: number;

  @Column()
  name: string;

  @Column({ default: true })
  isActive: boolean;
}

```

之后，需要在mudule中注册。在imports中用到`TypeOrmModule`的方法。

```typescript
import { Module } from "@nestjs/common";
import { OrmService } from "./orm.service";
import { OrmController } from "./orm.controller";
import { Orm } from "./entities/orm.entity";
import { TypeOrmModule } from "@nestjs/typeorm";

@Module({
  imports: [TypeOrmModule.forFeature([Orm])],
  controllers: [OrmController],
  providers: [OrmService],
})
export class OrmModule {}

```

重启nest，就可以自动创建表，上述示例的表为：

![image-20221126105051037](node.assets/image-20221126105051037.png)

如果需要修改数据库，就修改实体类再重启数据库就可以了。

```typescript
@PrimaryGeneratedColumn("uuid")
  id: number;
```

![image-20221126110304316](node.assets/image-20221126110304316.png)

对于它的crud操作，参考[小满nestjs（第二十六章 nestjs 第一个CURD）_小满zs的博客-CSDN博客](https://xiaoman.blog.csdn.net/article/details/127590610#comments_24296950)。

例如创建操作，首先在dto层定义在请求中的数据，比如这里只有一个name

```typescript
export class CreateOrmDto {
  name: string;
}

```

在dto层中也可以定义swagger的配置，以便在swagger页面中展示需要的参数，例如

```typescript
import { ApiProperty } from "@nestjs/swagger";

export class CreateOrmDto {
  @ApiProperty({ example: "ormName" })
  name: string;
}
```

在swagger中的展示如下：

![image-20221129152022618](node.assets/image-20221129152022618.png)

在service层的构造函数先创建数据仓库`constructor(@InjectRepository(Orm) private ormRepository: Repository<Orm>) {}`。

更改create函数，有点像django。

```typescript
  create(createOrmDto: CreateOrmDto) {
    const data: Orm = new Orm();
    data.name = createOrmDto.name;
    return this.ormRepository.save(data);
  }
```

整体service层代码参考

```typescript
import { Injectable } from "@nestjs/common";
import { CreateOrmDto } from "./dto/create-orm.dto";
import { UpdateOrmDto } from "./dto/update-orm.dto";
import { InjectRepository } from "@nestjs/typeorm";
import { Orm } from "./entities/orm.entity";
import { Repository } from "typeorm";

@Injectable()
export class OrmService {
  constructor(@InjectRepository(Orm) private ormRepository: Repository<Orm>) {}

  create(createOrmDto: CreateOrmDto) {
    const data: Orm = new Orm();
    data.name = createOrmDto.name;
    return this.ormRepository.save(data);
  }

  findAll() {
    return this.ormRepository.find();
  }

  findOne(id: number) {
    return `This action returns a #${id} orm`;
  }

  update(id: number, updateOrmDto: UpdateOrmDto) {
    return `This action updates a #${id} orm`;
  }

  remove(id: number) {
    return `This action removes a #${id} orm`;
  }
}

```

在发送了几次post请求后，检查数据库，数据已经添加

![image-20221128205224900](node.assets/image-20221128205224900.png)

### 微服务

为了保证各个子应用之间是相互独立的，所以在NestJS中也有微服务。可以参考这篇文章[Nest.js 的微服务，写起来也太简单了吧！ - 掘金 (juejin.cn)](https://juejin.cn/post/7207637337571901495)。微服务应用和主应用相互独立，它们之间使用TCP进行连接，如下所示：

![img](node.assets/fb35810637d44f54977cb76e89a13d6ftplv-k3u1fbpfcp-zoom-in-crop-mark1512000.awebp)

首先可以建立一个微服务的nest的应用。改造main.ts如下

```typescript
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { Transport, MicroserviceOptions } from '@nestjs/microservices';

async function bootstrap() {
  const app = await NestFactory.createMicroservice<MicroserviceOptions>(
    AppModule,
    {
      transport: Transport.TCP,
      options: {
        port: 8888,
      },
    },
  );
  await app.listen();
}

bootstrap();

```

这里表示启动一个微服务并且运行在8888端口上。之后，需要注册相关的微服务方法，注意这里与一般的网络请求注册不同。

```typescript
import { Controller } from '@nestjs/common';
import { MessagePattern } from '@nestjs/microservices';

@Controller()
export class AppController {
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  constructor() {}

  @MessagePattern('sum')
  sum(numArr: Array<number>): number {
    return numArr.reduce((total, item) => total + item, 0);
  }
}

```

比如上述代码中注册了sum的pattern。

在主应用中，如果需要连接这个微服务应用，在app中进行改造，需要注册它

```typescript
import { Module } from "@nestjs/common";
import { AppController } from "./app.controller";
import { AppService } from "./app.service";
import { U1Controller } from "./u1/u1.controller";
import { UserModule } from "./user/user.module";
import { ClientsModule, Transport } from "@nestjs/microservices";

@Module({
  imports: [
    ClientsModule.register([
      {
        name: "CALC_SERVICE",
        transport: Transport.TCP,
        options: {
          port: 8888,
        },
      },
    ]),
    UserModule,
  ],
  controllers: [AppController, U1Controller],
  providers: [AppService],
})
export class AppModule {}

```

之后，就可以根据注册的名字添加关于微服务的初始化，然后就可以使用

```typescript
import { Controller, Get, Inject, Query } from "@nestjs/common";
import { AppService } from "./app.service";
import { ClientProxy } from "@nestjs/microservices";
import { Observable } from "rxjs";

@Controller("")
export class AppController {
  constructor(
    private readonly appService: AppService,
    @Inject("CALC_SERVICE") private calcClient: ClientProxy,
  ) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @Get("/ser1")
  getServer1(@Query("num") str): Observable<number> {
    const numArr = str.split(",").map((i) => parseInt(i));
    return this.calcClient.send("sum", numArr);
  }
}

```

![image-20230405101001259](node.assets/image-20230405101001259.png)

### 网络请求

网络请求分五种：

+ url param

  就是RestfulAPI模式，将参数写在url里面

+ query

  就是使用`?`来标识参数

  ```javascript
  const queryString = require('query-string');
  
  queryString.stringify({
    name: '光',
    age: 20
  });
  
  // ?name=%E5%85%89&age=20
  ```

+ form-urlencoded

  是form提交数据的。需要指定下 content-type 是 `application/x-www-form-urlencoded`。

  ```html
  <!DOCTYPE html>
  <html lang="en">
  <head>
      <script src="https://unpkg.com/axios@0.24.0/dist/axios.min.js"></script>
      <script src="https://unpkg.com/qs@6.10.2/dist/qs.js"></script>
  </head>
  <body>
      <script>
          async function formUrlEncoded() {
              const res = await axios.post('/api/person', Qs.stringify({
                  name: '光',
                  age: 20
              }), {
                  headers: { 'content-type': 'application/x-www-form-urlencoded' }
              });
              console.log(res);  
          }
  
          formUrlEncoded();
      </script>
  </body>
  </html>
  ```

+ form-data

  用于发文件，前端代码使用 axios 发送 post 请求，指定 content type 为 `multipart/form-data`：

  ```html
  <!DOCTYPE html>
  <html lang="en">
  <head>
      <script src="https://unpkg.com/axios@0.24.0/dist/axios.min.js"></script>
  </head>
  <body>
      <input id="fileInput" type="file" multiple/>
      <script>
          const fileInput = document.querySelector('#fileInput');
  
          async function formData() {
              const data = new FormData();
              data.set('name','光');
              data.set('age', 20);
              data.set('file1', fileInput.files[0]);
              data.set('file2', fileInput.files[1]);
  
              const res = await axios.post('/api/person/file', data, {
                  headers: { 'content-type': 'multipart/form-data' }
              });
              console.log(res);     
          }
  
          fileInput.onchange = formData;
      </script>
  </body>
  </html>
  
  ```

  获取数据的时候可以这样

  ```typescript
  import { AnyFilesInterceptor } from '@nestjs/platform-express';
  import { CreatePersonDto } from './dto/create-person.dto';
  
  @Controller('api/person')
  export class PersonController {
    @Post('file')
    @UseInterceptors(AnyFilesInterceptor())
    body2(@Body() createPersonDto: CreatePersonDto, @UploadedFiles() files: Array<Express.Multer.File>) {
      console.log(files);
      return `received: ${JSON.stringify(createPersonDto)}`
    }
  }
  ```

+ json

  form-urlencoded 需要对内容做 url encode，而 form data 则需要加很长的 boundary，两种方式都有一些缺点。如果只是传输 json 数据的话，不需要用这两种。这里不需要指定content type 为`application/json`，默认进行的。

  ```html
  <!DOCTYPE html>
  <html lang="en">
  <head>
      <script src="https://unpkg.com/axios@0.24.0/dist/axios.min.js"></script>
  </head>
  <body>
      <script>
          async function json() {
              const res = await axios.post('/api/person', {
                  name: '光',
                  age: 20
              });
              console.log(res);     
          }
          json();
      </script>
  </body>
  </html>
  
  ```

### pipe

pipe有全局的pipe和局部的pipe两种。其中pipe 更多还是单独在某个参数的位置应用，如下图所示：

![img](node.assets/5fced92c2344495b86524871d8ed9cfatplv-k3u1fbpfcp-zoom-in-crop-mark3024000.awebp)

它还能检测get请求所携带的的参数是否符合要求，如下：

```typescript
  @Get()
  @Version("3")
  fundAll3(@Query("a", ParseBoolPipe) a: boolean, @Query() query) {
    console.log(query);
    return {
      code: 200,
      message: query.name,
      flag: a,
    };
  }
```

![image-20230707001620335](node.assets/image-20230707001620335.png)

### 重要参数装饰器

#### Ip

可以使用这个装饰器来获取IP，如下所示

```typescript
  @Get()
  @Version("2")
  findAll2(@Ip() ip: string) {
    return "version 2 find all from " + ip;
  }
```

![image-20230707002059406](node.assets/image-20230707002059406.png)

## WebRTC

WebRTC支持**实时双向音视频、主流浏览器支持、开发者容易入手、使用范围广且技术开源成熟**。参考小册https://juejin.cn/book/7168418382318927880

### 获取视频流

```typescript
  async function getLocalStream(constraints: MediaStreamConstraints) {
    // 获取媒体流
    const stream = await navigator.mediaDevices.getUserMedia(constraints);

    // 获取screen媒体流
    // const stream = await navigator.mediaDevices.getDisplayMedia(constraints)
    // 将媒体流设置到 video 标签上播放
    playLocalStream(stream);
  }

  // 播放本地视频流
  function playLocalStream(stream: MediaStream) {
    const videoEl = document.getElementById("localVideo") as HTMLVideoElement;
    videoEl.srcObject = stream;
  }

  getLocalStream({
    audio: false,
    video: true,
  });
```

这里可以获取本地的摄像头的视频流，然后在这个标签`<video id="localVideo" autoplay playsinline muted></video>`中播放数据流。如下所示。*（所以我是一只猫吗？*

![image-20230419191204533](node.assets/image-20230419191204533.png)

### 连接

`PeerConnection`可以说是整个`WebRTC`通话的载体，如果没有这个对象，那么后面所有流程都是没法进行的。

![img](node.assets/48a9cc63f3fc4f7d8f1778b2a19baf77tplv-k3u1fbpfcp-zoom-in-crop-mark3024000.awebp)

对照这个流程图，称上图中 **A** 为**caller（呼叫端），B为callee（被呼叫端）。**

1. 首先 A 呼叫 B，呼叫之前我们一般通过实时通信协议`WebSocket`即可，让对方能收到信息。

2. B 接受应答，A 和 B 均开始初始化`PeerConnection `实例，用来关联 A 和 B 的`SDP`会话信息。

3. A 调用`createOffer`创建信令，同时通过`setLocalDescription`方法在本地实例`PeerConnection`中储存一份（**图中流程①**）。

4. 然后调用信令服务器将 A 的`SDP`转发给 B（**图中流程②**）。

5. B 接收到 A 的`SDP`后调用`setRemoteDescription`，将其储存在初始化好的`PeerConnection`实例中（**图中流程③**）。

6. B 同时调用`createAnswer`创建应答`SDP`，并调用`setLocalDescription`储存在自己本地`PeerConnection`实例中（**图中流程④**）。

7. B 继续将自己创建的应答`SDP`通过服务器转发给 A（**图中流程⑤**）。

8. A 调用`setRemoteDescription`将 B 的`SDP`储存在本地`PeerConnection`实例（**图中流程⑤**）。

9. 在会话的同时，从图中我们可以发现有个`ice candidate`，这个信息就是 ice 候选信息，A 发给 B 的 B 储存，B 发给 A 的 A 储存，直至候选完成。

