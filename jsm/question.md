# 前端面经问题

在此记录面经问题，主要内容为前端，网络资源可以参考[web前端面试 - 面试官系列 (vue3js.cn)](https://vue3js.cn/interview/)。算法部分请看[这里](../mlearning/question.md)

---

## Node

1. nodejs中的path.resolve和path.join区别

   对于`path.resolve()`来说，是直接返回绝对路径的，并且如果参数中有'../'或者'./'开头的，就也会做拼接。而`path.join()`是直接做字符串的拼接。参考链接http://once-and-only.com/programing/javascript/path-resolve%E3%81%AE%E4%BD%BF%E3%81%84%E6%96%B9/。

2. 路由的两种实现方式？

   主要是hash模式和history模式，参考[面试官为啥总是喜欢问前端路由实现方式？ - 掘金 (juejin.cn)](https://juejin.cn/post/7127143415879303204)

3. Javascript的事件循环是什么？

   1. 在此次 tick 中选择最先进入队列的任务( oldest task )，如果有则执行(一次)

   2. 检查是否存在 Microtasks ，如果存在则不停地执行，直至清空Microtask Queue

   3. 更新 render

   4. 主线程重复执行上述步骤
   
4. 宏任务和微任务分别有哪些？
   
   1. 宏任务主要包含：script( 整体代码)、setTimeout、setInterval、I/O、UI 交互事件、setImmediate(Node.js 环境)
   2. 微任务主要包含：Promise、MutaionObserver、process.nextTick(Node.js 环境)

5. setTimeOut是否可靠？

   javascript引擎只有一个线程，迫使异步事件只能加入队列去等待执行。
    在执行异步代码的时候，setTimeout 和setInterval 是有着本质区别的。
    如果计时器被正在执行的代码阻塞了，它将会进入队列的尾部去等待执行直到下一次可能执行的时间出现（可能超过设定的延时时间）。
    如果interval回调函数执行需要花很长时间的话(比指定的延时长)，interval有可能没有延迟背靠背地执行。

6. 在使用threejs展示模型的时候，如果出现了模型的某些面突然变黑的问题，应该如何定位问题？

   考虑使用抓帧的办法

7. 前端的网页出现了内存泄漏的问题，该如何解决？

   可能的原因：

   + 全局变量的滥用：全局变量在整个应用程序中都是可见的，如果不正确地使用，很容易导致内存泄漏。
   + 闭包：在JavaScript中，闭包可以引用外部函数的变量，如果闭包引用了一个大对象，那么这个对象就不能被垃圾回收机制回收，从而导致内存泄漏。
   + 没有及时清理事件监听器：如果事件监听器没有被及时清理，那么它们会一直存在，从而导致内存泄漏。
   + 定时器：如果定时器没有被及时清除，那么它们会一直存在，从而导致内存泄漏。

8. tree-shaking的原理是什么？

   1. **静态分析代码**: Webpack会对代码进行静态分析，识别出哪些代码被引入，哪些代码未被引入。
   2. **标记未使用代码**: 对于未被引入的代码，Webpack会进行标记。
   3. **删除未使用代码**: 在生成最终的打包文件时，Webpack会删除被标记为未使用的代码，从而减小打包文件的体积。

9. Nodejs中的拦截器一般有什么用？

## Vue

1. Vue发送网络请求的生命阶段是？

   *出自原创科技技术一面*

   mounted和created两个阶段。

   考虑它们的不同，created阶段更前，在此阶段还未挂载但是能操作``$data``。

2. Vue如何封装未知层数的树型组件？

   *出自原创科技一面*

   使用v-for

3. Vue的父子节点的双向绑定及其意义？

   *出自原创科技一面*

4. vue中computed和watch的区别？

   *出自寝室扯淡*

   computed中使用了异步的函数也不能用不了。

   如果要使用异步的函数，那么就必须使用watch。

   并且在组件中使用computed的使用，就计算一次渲染多次。

5. 谈谈对于vue的keep-alive的理解？

   出自B站视频[链接](https://www.bilibili.com/video/BV1Tu411B7mv/?spm_id_from=333.788&vd_source=36542d6c49bf487d8a18d22be404b8d2)

   首先，这个标签是vue自带的，用于缓存组件。在这个标签里面的组件在第二次以及之后进入之后，只会执行activated生命周期。

   它的用途可以放在对于网络请求数量的减少，减轻后端服务器的压力。

6. vue中的v-if和v-show的区别？

   在v-if来说，这个是是否渲染出这个盒子的，即创建这个dom节点。对于v-show来说，是在渲染之后是否展示，即操作display属性。如果是要频繁切换，就是用v-show更好。

7. vue中的样式穿透如何实现？

   这里问的是在scoped的style标签里面渲染某个外面的样式。参考scss的做法，使用``父元素 /deep/ 子元素``的方法选取到那个标签。

8. 讨论一下vue2中的数据劫持实现原理？

   参考视频[链接](https://www.bilibili.com/video/BV1mf4y1Q7mg?spm_id_from=333.337.search-card.all.click&vd_source=36542d6c49bf487d8a18d22be404b8d2)

   主要是使用`Object.defineProperty`

9. 讨论一下vue3中的数据劫持实现原理？

   参考视频[链接](https://www.bilibili.com/video/BV1Wy4y1y7vb?p=2&vd_source=36542d6c49bf487d8a18d22be404b8d2)

   使用proxy

10. 为什么Vue3的数据劫持改成了proxy？

   + 无法检测到对象属性的`新增`或``删除``（vue2提供了vue.set方法来解决）
   + 深度监听，层层处理，影响性能
   + 当对某个数组arr[index] = val这样赋值时,无法监听数组变化

10. 谈谈vue2中对于数组的劫持？

    数组就是使用` object.defineProperty` 重新定义数组的每一项，那能引起数组变化的方法我们都是知道的，` pop` 、` push` 、` shift` 、` unshift` 、` splice` 、` sort` 、` reverse` 这七种，只要这些方法执行改了数组内容，我就更新内容就好了，是不是很好理解。

    1. 是用来函数劫持的方式，重写了数组方法，具体呢就是更改了数组的原型，更改成自己的，用户调数组的一些方法的时候，走的就是自己的方法，然后通知视图去更新。
    2. 数组里每一项可能是对象，那么我就是会对数组的每一项进行观测，（且只有数组里的对象才能进行观测，观测过的也不会进行观测）

11. vue2组件中的data为什么是函数？

    <details>
    <summary>展开查看</summary>
    <pre>
    避免组件中的数据互相影响。同一个组件被复用多次会创建多个实例，如果<code>data</code>是一个对象的话，这些实例用的是同一个构造函数。为了保证组件的数据独立，要求每个组件都必须通过<code>data</code>函数返回一个对象作为组件的状态。
    </pre>
    </details>

12. react与vue在fiber上的区别？

    [阿里三面：灵魂拷问——有react fiber，为什么不需要vue fiber？_frontend_frank的博客-CSDN博客](https://blog.csdn.net/frontend_frank/article/details/123700502)

    react因为先天的不足——无法精确更新，所以需要react fiber把组件渲染工作切片；而vue基于数据劫持，更新粒度很小，没有这个压力；

    react fiber这种数据结构使得节点可以回溯到其父节点，只要保留下中断的节点索引，就可以恢复之前的工作进度；

14. vuex与pinia的区别？

    pinia没有mutation,他只有state，getters，action【同步、异步】使用来修改state数据.
    
14. 解释一下Diff算法。

    Diff 算法是 Vue.js 中用于比较虚拟 DOM 树和真实 DOM 树差异的核心算法。Vue.js 在每次数据变动时，都会使用 Diff 算法来计算虚拟 DOM 树和真实 DOM 树之间的差异，并生成一个差异对象（patch）。然后，Vue.js 会根据这个差异对象，对真实 DOM 进行最小化的更新。

    Diff 算法的工作原理如下：

    1. **同层级比较**：Diff 算法只会在同层级进行节点比较，不会进行跨层级的比较。这样可以降低算法的复杂度，提高性能。
    2. **key 值唯一标识**：Vue.js 会为每个节点分配一个唯一的 key 值，用于在 Diff 过程中快速定位节点。如果节点的 key 值发生改变，那么该节点会被视为一个新节点，从而进行完全的替换。
    3. **列表优化**：当需要渲染的列表元素数量较大时，Vue.js 会采用一种基于索引的高效算法来比较列表元素，以提高渲染性能。

    `vue3`在`diff`算法中相比`vue2`增加了静态标记：

    关于这个静态标记，其作用是为了会发生变化的地方添加一个`flag`标记，下次发生变化的时候直接找该地方进行比较。

15. 解释一下Vue中的双向绑定原理。

    参考vue的数据劫持。

16. Vue的nextTick是什么？

    我们可以理解成，`Vue` 在更新 `DOM` 时是异步执行的。当数据发生变化，`Vue`将开启一个异步更新队列，视图需要等队列中所有数据变化完成之后，再统一进行更新。这是一种优化策略。

17. vue3的tree-shaking怎么实现的？

    [面试官：说说Vue 3.0中Treeshaking特性？举例说明一下？ | web前端面试 - 面试官系列 (vue3js.cn)](https://vue3js.cn/interview/vue3/treeshaking.html#一、是什么)

    `Tree shaking`无非就是做了两件事：

    - 编译阶段利用`ES6 Module`判断哪些模块已经加载
    - 判断那些模块和变量未被使用或者引用，进而删除对应代码

18. vueRouter 原理是什么？

    在 Web 前端单页应用 SPA(Single Page Application)中，路由描述的是 URL 与 UI 之间的映射关系，这种映射是单向的，即 URL 变化引起 UI 更新（无需刷新页面）。

19. vueRouter 实现方法

    先获取mode的值，如果mode的值为`history`但是浏览器不支持`history`模式,那么就强制设置mode值为`hash`。如果支持则为`history`。接下来，根据mode的值，来选择vue-router使用哪种模式。

    hash虽然出现在URL中，但不会被包括在HTTP请求中。它是用来指导浏览器动作的，对服务器端完全无用，因此，改变hash不会重新加载页面。

20. vite剪枝的原理是什么？

    

​    

## React

1. React Fiber 原理是什么？

   参考[react.js - React Fiber 原理介绍 - React那点事儿 - SegmentFault 思否](https://segmentfault.com/a/1190000040591546)

   大量的同步计算任务阻塞了浏览器的 UI 渲染。当我们调用setState更新页面的时候，React 会遍历应用的所有节点，计算出差异，然后再更新 UI。整个过程是一气呵成，不能被打断的。如果页面元素很多，整个过程占用的时机就可能超过 16 毫秒，就容易出现掉帧的现象。--React15

   Fiber 其实指的是一种数据结构，它可以用一个纯 JS 对象来表示：

   ```javascript
   const fiber = {
       stateNode,    // 节点实例
       child,        // 子节点
       sibling,      // 兄弟节点
       return,       // 父节点
   }
   ```

2. useRef的使用？

   ```javascript
   // 在函数内部
   const handleRefUpdate = () => {
       // 访问被引用对象的值
       const value = refObj.current
       // 更新被引用对象的值
      refObj.current = newValue
   }
   ```

   - 被引用对象的值在重新渲染之间保持不变。
   - 更新被引用对象的值不会触发重新渲染。

   同时，useRef也可以对应一个DOM对象

   ```javascript
   import {useRef} from ‘react’
   
   const myComponent = () => {
     const elementRef = useRef()
     
     return (
    		<input ref={elementRef} type="text" />
     )
   }
   ```

3. React 有哪些性能优化策略？

   useMemo & useCallback

   useMemo 是 React 的一个 Hooks，用来缓存计算结果。在不使用 useMemo 的情况下，当组件重新渲染时，会重复计算渲染所用到的值，可能会导致渲染缓慢。useMemo 可以缓存这些值，只有当某些 state 变化后，才会重新计算结果。这样可以最大程度降低额外的计算。useCallback 也可以实现类似的效果，它的作用是缓存函数。

4. React两种组件形式的区别？

   针对两种React组件，其区别主要分成以下几大方向：

   class组件是有状态的组件，可以定义state状态。函数组件是无状态的。

   class组件有生命周期。函数组件没有生命周期，函数组件使用的是Hooks。

   class组件是有this对象的。函数组件没有this对象。

   组件调用：class组件实例化后调用render方式调用。函数组件是直接调用的。

   class组件内部render方法return返回渲染jsx模板。函数组件直接返回即可。

   ref获取子组件的对象。class组件可以直接获取到，函数组件无法直接获取到。

   绑定bind改变this指向，只适用于class组件。

5. 为什么react需要引入hooks？

   1. **组件之间一些包含状态的逻辑很难拆分复用**
   2. **复杂的大型组件越发难以理解和维护**，对应生命周期
   3. **组件类的方式对开发者和机器都不友好**

6. react的fiber的diff算法为什么会比react16之前更快？

6. 



## CSS3 ES6 HTML5

1. iframe的缺点？

   *出自《前端程序员》*

   iframe由如下缺点：

   + iframe会阻塞主页面的onload的事件
   + iframe的元素即使为空，加载也需要时间
   + iframe元素没有语义

2. DOCTYPE和区分严格模式与混杂模式的意义在于？
   
   *出自《前端程序员》*
   
   ``<!DOCTYPE>``来表示严格模式。否则是混杂模式。
   
3. display的各个值有什么意义？

   *出自《前端程序员》*

4. 解释一下es6新增的proxy和reflect语法？

   使用`new Proxy(target, handler)`创建代理，并且，handler表示的是捕获器。

   proxy的基本方法

   ```javascript
    const obj = {
       name: "copyer",
       age: 12,
    };
    
    const objProxy = new Proxy(obj, {
        /**
         * @param {*} target :目标对象
         * @param {*} key : 键值
         * @param {*} receiver ：代理对象
         */
        get: function (target, key, receiver) {
          console.log("get捕捉器");
          return target[key];
        },
    });
    
    console.log(objProxy.name); // copyer
   ```

   以上的receiver为代理对象objProxy的this的值。可以在操作的时候的使用到这个对象。

   同理，对于proxy的set方法为：

   ```javascript
    const obj = {
      name: "copyer",
      age: 12,
    };
    ​
    const objProxy = new Proxy(obj, {
      /**
       * @param {*} target : 目标对象
       * @param {*} key ：键值
       * @param {*} newValue ：新增
       * @param {*} receiver ：代理对象
       */
      set: function (target, key, newValue, receiver) {
        console.log("set捕捉器");
        target[key] = newValue;
      },
    });
    
    objProxy.age = 23;
    console.log(obj.age); // 23
   ```

   它们两个配合使用

   ```javascript
    const obj = {
      name: "copyer",
      age: 12,
    };
    const objProxy = new Proxy(obj, {
      get: function (target, key, receiver) {
        console.log("get捕捉器");
        return target[key];
      },
    });
    console.log(objProxy.name); // copyer
   ```

   ```javascript
    const obj = {
      name: "copyer",
      age: 12,
    };
    
    const objProxy = new Proxy(obj, {
      get: function (target, key, receiver) {
        console.log("get捕获器");
        return Reflect.get(target, key);
      },
      set: function (target, key, newValue, receiver) {
        console.log("set捕获器");
        return Reflect.set(target, key, newValue);
      },
      has: function (target, key) {
        console.log("has捕获器");
        return Reflect.has(target, key);
      },
      deleteProperty: function (target, key) {
        console.log("deleteProperty捕获器");
        return Reflect.deleteProperty(target, key);
      },
      getPrototypeOf: function (target) {
        console.log("getPrototypeOf捕获器");
        return Reflect.getPrototypeOf(target);
      },
      setPrototypeOf: function (target, newObj) {
        return Reflect.setPrototypeOf(target, newObj);
      },
      isExtensible: function (target) {
        console.log("isExtensible捕获器");
        return Reflect.isExtensible(target);
      },
      preventExtensions: function (target) {
        console.log("preventExtensions捕捉器");
        return Reflect.preventExtensions(target);
      },
      getOwnPropertyDescriptor: function (target) {
        console.log("getOwnPropertyDescriptor捕捉器");
        return Reflect.getOwnPropertyDescriptor(target);
      },
      defineProperty: function (target, key, obj) {
        console.log("defineProperty捕捉器");
        return Reflect.defineProperty(target, key, obj);
      },
      ownKeys: function (target, key) {
        console.log("ownKeys捕捉器");
        return Reflect.ownKeys(target, key);
      },
    });
   ```

5. JS的深拷贝与浅拷贝的区别与意义？

   *出自原创科技一面*

   *参考Vue3的笔记*

6. 有两个对象数组A和B，A对应着员工ID和员工工资，B对应着员工ID和员工绩效。如何返回一个对象数组，使得对应着员工的ID和员工的工资加绩效？

   *出自原创科技技术一面*

   使用map来记录员工的ID。比set好在能直接对值进行操作。

7. 实现深拷贝的方法？

   *出自飞书*

   1. 使用``JSON.parse(JSON.stringify());``但是对于子对象就很难使用了

   2. 使用判断和递归的方法，判断拷贝的对象是否是数组或者Object

      ```javascript
      module.exports = function clone(target) {
          if (typeof target === 'object') {
              let cloneTarget = Array.isArray(target) ? [] : {};
              for (const key in target) {
                  cloneTarget[key] = clone(target[key]);
              }
              return cloneTarget;
          } else {
              return target;
          }
      };
      ```

      但是这样解决不了循环引用的问题，例如如下对象

      ```javascript
      const target = {
          field1: 1,
          field2: undefined,
          field3: {
              child: 'child'
          },
          field4: [2, 4, 8]
      };
      target.target = target;
      ```

   3. 使用map来解决上述问题

      ```javascript
      function clone(target, map = new Map()) {
          if (typeof target === 'object') {
              let cloneTarget = Array.isArray(target) ? [] : {};
              if (map.get(target)) {
                  return target;
              }
              map.set(target, cloneTarget);
              for (const key in target) {
                  cloneTarget[key] = clone(target[key], map);
              }
              return cloneTarget;
          } else {
              return target;
          }
      };
      ```

8. 如何实现add(1)(2)(3)

   *出自飞书*

   考虑函数柯里化：函数柯里化（curry）是函数式编程里面的概念。curry的概念很简单：只传递给函数一部分参数来调用它，让它返回一个函数去处理剩下的参数。

   ```javascript
   const add = x => y => z => x + y + z;
   console.log(add(1)(2)(3));
   ```

   但是如果想要实现

   ```javascript
   add(1, 2, 3);
   add(1, 2)(3);
   add(1)(2, 3);
   ```

   这就需要考虑判断参数个数，可以拓展参数

   ```javascript
   const curry = (fn, ...args) => 
       // 函数的参数个数可以直接通过函数数的.length属性来访问
       args.length >= fn.length // 这个判断很关键！！！
       // 传入的参数大于等于原始函数fn的参数个数，则直接执行该函数
       ? fn(...args)
       /**
        * 传入的参数小于原始函数fn的参数个数时
        * 则继续对当前函数进行柯里化，返回一个接受所有参数（当前参数和剩余参数） 的函数
       */
       : (..._args) => curry(fn, ...args, ..._args);
   
   function add1(x, y, z) {
       return x + y + z;
   }
   const add = curry(add1);
   console.log(add(1, 2, 3));
   console.log(add(1)(2)(3));
   console.log(add(1, 2)(3));
   console.log(add(1)(2, 3));
   ```

9. JS实现大数相加

   *出自飞书*

   [JS 实现两个大数相加？ - 知乎 (zhihu.com)](https://zhuanlan.zhihu.com/p/72179476)

10. 实现数组的flat操作

    *出自飞书*

    [面试官连环追问：数组拍平（扁平化） flat 方法实现 - SegmentFault 思否](https://segmentfault.com/a/1190000021366004)

    - **第一个要解决的就是遍历数组的每一个元素；**
    - **第二个要解决的就是判断元素是否是数组；**
    - **第三个要解决的就是将数组的元素展开一层**

11. CSS的引入方式？link与@import的区别？

    *出自《前端程序员》*

    有三种引入方式：

    + 行内式，将样式写在元素的style里面。
    + 内嵌式，将样式写在style元素里面。
    + 外链式，使用link引入。

    link与@import的区别：

    + @import只能加载css
    + 如果使用link，在页面载入的同时就会加载，是同步的。@import加载是等待网页完全载入之后再加载css文件，是异步加载。
    + link没有兼容问题，@import在低版本的浏览器中不支持
    + link是DOM元素，支持使用JavaScript控制DOM和样式，@import不支持。

12. html canvas 绘制1px 出现模糊的原因是什么？

    这是由于绘制时，将画笔中线对齐坐标轴线，必然有一半线宽在坐标轴线之外，这一半的线宽，如果不是整数，会被自动加满，使之正好对齐一个像素点，由于本身是不满这1个像素的，所以用了更浅的颜色来表示（改变透明度），导致模糊。（因为1像素就是最小单位了，0.x的像素不能显示）

12. 拍平二维数值？

    使用apply函数

    ```javascript
    function flat(a){
        return Array.prototype.concat.apply([],a)
    }
    ```

13. 拍平多维数组？

    使用递归的，使用``Array.prototype.isArray``方法来判断数组，进行递归

    ```javascript
    Array.prototype.flat2 =function(){
      let reslut=[]
      for(let i=0;i<this.length;i++){
          if(Array.isArray(this[i])){
              reslut=reslut.concat(this[i].flat2())
          }else{
              reslut.push(this[i])
          }
      } 
      return reslut
    }
    let arr=[1,[2,[3,[4],[5],[6]]]]
    arr.flat2()
    ```

    使用生成器迭代法

    ```javascript
        function *flat(arr) {
           for (const item of arr) {
               if (Array.isArray(item)) {
                   yield *flat(item);
               } else {
                   yield item;
               }
           }
       }
       const result = [...flat([1,2,[3,[4]]])]
       console.log(result) //  [1, 2, 3, 4]
    ```

14. 如何实现一个方法，使得它能在一个对象中取值并组成一个新的对象？

    参考only库的代码

    ```javascript
    module.exports = function(obj, keys){
      obj = obj || {};
      if ('string' == typeof keys) keys = keys.split(/ +/);
      return keys.reduce(function(ret, key){
        if (null == obj[key]) return ret;
        ret[key] = obj[key];
        return ret;
      }, {});
    };
    ```

15. 装饰器如何实现？

    参考[第三章（前置知识-装饰器）_哔哩哔哩_bilibili](https://www.bilibili.com/video/BV1NG41187Bs?p=3&vd_source=36542d6c49bf487d8a18d22be404b8d2)，类似于Java中的注解，JavaScript的装饰器是如何实现的？

    需要分别讨论类装饰器，属性装饰器等。并且需要注意的是，装饰器实际上就是一个函数的语法糖表示。

    需要注意的是，如果要使用typescript的装饰器，需要打开tsconfig的设置`"experimentalDecorators": true`。

    + 类构造器

      使用`ClassDecorator`，可以在参数中获取到所装饰类的构造函数。

      如下所示，可以打印出这里的实例化的类

      ```typescript
      const doc: ClassDecorator = (target: any) => {
          console.log(target);
      }
      
      @doc
      class Masaikk {
          constructor() {
          }
      }
      
      let me = new Masaikk();
      
      ```

      同时，也可以通过原型链给这个类添加属性。

      ```typescript
      const doc: ClassDecorator = (target: any) => {
          console.log(target);
          target.prototype.name = 'masaikk';
      }
      
      @doc
      class Masaikk {
          constructor() {
          }
      }
      
      let me: any = new Masaikk();
      
      console.log(me.name);
      
      ```

      ![image-20221120000926384](question.assets/image-20221120000926384.png)

    + 属性装饰器

      使用类型`PropertyDecorator`，接受两个参数，分别是target和key。`declare type PropertyDecorator = (target: Object, propertyKey: string | symbol) => void;`此时的target是指向原型对象的，而key是指向所装饰的属性的名字。比如如下所示：

      ```typescript
      const doc: PropertyDecorator = (target: any, key: string | symbol) => {
          console.log(target);
          console.log()
      }
      
      
      class Masaikk {
          @doc
          public name:string;
      
          constructor() {
              this.name = 'masaikk'
          }
      }
      
      let me: any = new Masaikk();
      
      console.log(me.name);
      
      ```

      ![image-20221120001614195](question.assets/image-20221120001614195.png)

    + 方法装饰器

      使用`MethodDecorator`，并且如果想要实现装饰器传参的用法，需要借助函数柯里化的知识。函数签名`declare type MethodDecorator = <T>(target: Object, propertyKey: string | symbol, descriptor: TypedPropertyDescriptor<T>) => TypedPropertyDescriptor<T> | void;`第三个参数为描述符。

      ```typescript
      const doc: MethodDecorator = (target: any, key: string | symbol, descriptor: any) => {
          console.log(target);
          console.log(key);
          console.log(descriptor);
      }
      
      
      class Masaikk {
          public name: string;
          constructor() {
              this.name = 'masaikk';
          }
      
          @doc
          saySomething() {
              console.log("你好" + this.name);
          }
      }
      
      let me: any = new Masaikk();
      
      ```

      ![image-20221120113320525](question.assets/image-20221120113320525.png)

      如果想要实现装饰器传参，就要使用装饰器工厂的概念，也就是函数柯里化。类似与

      ```typescript
      const dl: any = (mess: string) => {
          return (target: any, key: string | symbol, descriptor: any) => {
              let fc = descriptor.value;
              fc(mess);
          }
      }
      
      class Masaikk {
          public name: string;
      
          constructor() {
              this.name = 'masaikk';
          }
      
          @doc
          saySomething() {
              console.log("你好" + this.name);
          }
      
          @dl('masakk')
          say(mess: string) {
              console.log("dl 装饰器 " + mess);
          }
      }
      
      let me: any = new Masaikk();
      
      ```

      通过描述符的value获取到函数，再调用它。

      ![image-20221120114802981](question.assets/image-20221120114802981.png)

    + 参数装饰器

      使用`ParameterDecorator`函数签名是`declare type ParameterDecorator = (target: Object, propertyKey: string | symbol, parameterIndex: number) => void;`

      在这里的第三个参数是index，是参数的位置

      ```typescript
      const docPara: ParameterDecorator = (target: any, key: string | symbol, index: any) => {
          console.log(target);
          console.log(key);
          console.log(index);
      }
      
      class Masaikk {
          public name: string;
      
          constructor() {
              this.name = 'masaikk';
          }
          
          saySomething() {
              console.log("你好" + this.name);
          }
          
          say(@docPara mess: string) {
              console.log("dl 装饰器 " + mess);
          }
      }
      
      let me: any = new Masaikk();
      
      ```

      ![image-20221120200845859](question.assets/image-20221120200845859.png)

16. 解释一下前端HTML中的JavaScript加载问题？

    参考链接[你不知道的 DOMContentLoaded - 知乎 (zhihu.com)](https://zhuanlan.zhihu.com/p/25876048)

    解释一下，当 HTML 文档解析完成就会触发 DOMContentLoaded，而所有资源加载完成之后，load 事件才会被触发。

    另外需要提一下的是，我们在 jQuery 中经常使用的 `(document).ready(function() { });` 其实监听的就是 DOMContentLoaded 事件，而 `(document).load(function() {});` 监听的是 load 事件。

17. 元素居中的方式有哪些？

    1. 使用margin进行固定长度的偏移
    2. 使用绝对定位并进行偏移
    3. flex
    4. 使用table-cell进行居中显示
    
18. JS 的 new 做了哪些事情呢？

    1. 首先创建一个新的空对象
    2. 然后将空对象的 `__proto__` 指向构造函数的原型
       1. 它将新生成的对象的 `__proto__` 属性赋值为构造函数的 `prototype` 属性，使得通过构造函数创建的所有对象可以共享相同的原型。
    
       2. 这意味着同一个构造函数创建的所有对象都继承自一个相同的对象，因此它们都是同一个类的对象。
    
    
     3.  改变 `this` 的指向，指向空对象
     3.  对构造函数的返回值做判断，然后返回对应的值
    
19. 箭头函数的this指向？

    箭头函数的this指向父级作用域。

20. for let 和 for var的区别

    [JS踩坑: for let 和 for var的区别_js for var-CSDN博客](https://blog.csdn.net/xingchenxuanfeng/article/details/132647023)

21. interface和type的区别？

    1.  类型别名可以用于其它类型 （联合类型、元组类型、交叉类型），interface不支持这些，interface支持对象类型定义更好。

    2. interface 可以多次定义 并被视为合并所有声明成员 type 不支持

    3. type 能使用 in 关键字生成映射类型，但 interface 不行

    4. 默认导出方式不同

       ```typescript
       // inerface 支持同时声明，默认导出 而type必须先声明后导出
       export default interface Config {
         name: string;
       }
       // 同一个js模块只能存在一个默认导出哦
        type Config2 = {name: string}
         export default Config2
       ```

22. undefined和null的底层实现分别是什么？

    问：那什么时候赋null值合适？
    一、当这个变量将来会用来保存对象的话，最好是先为这个变量初始化null值。java精神：万物皆对象。换句话，我们应该明确地为对象变量空值的情况下初始为null，以方便区分undefined和null；
    二、当这个变量准备弃用时，应该为变量赋null值，这在js中叫作解除引用。解除引用能让变量脱离执行环境，进一步回收内存。这在实战中能够防止变量循环引用，重复混乱地赋值。

    最后作一个简单的undefined和null的对比：

    + undefined表示变量未定义或未赋值的初始状态值，是一个机器式标识；
    + null表示一个空对象变量，本身是空对象指针，是一个程序式标识。

23. proxy和reflect的使用场景

    `Proxy`方法拦截`target`对象的属性赋值行为。它采用`Reflect.set`方法将值赋值给对象的属性，确保完成原有的行为，然后再部署额外的功能（比如这里的打印信息）, 这个额外的功能又叫**代理陷阱**。

24. JavaScript如何判断一个元素是否在可视区域中？

    计算offsetTop、scrollTop

    ```javascript
    function isInViewPortOfOne (el) {
        // viewPortHeight 兼容所有浏览器写法
        const viewPortHeight = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight 
        const offsetTop = el.offsetTop
        const scrollTop = document.documentElement.scrollTop
        const top = offsetTop - scrollTop
        return top <= viewPortHeight
    }
    ```

25. 进程间有哪些通信方式?

    + 管道（也称作共享文件）
    + 消息队列（也称作消息传递）
    + 共享内存（也称作共享存储）
    + 信号量和 PV 操作
    + 信号
    + 套接字（Socket）

26. Web Worker 是什么？

    HTML5 提供并规范了 Web Worker 这样一套 API，它允许一段 JavaScript 程序运行在主线程之外的另外一个线程（Worker 线程）中。
    Web Worker 的作用，就是为 JavaScript 创造多线程环境，允许主线程创建 Worker 线程，将一些任务分配给后者运行。这样的好处是，一些计算密集型或高延迟的任务，被 Worker 线程负担了，主线程就会很流畅，不会被阻塞或拖慢。
    
27. ES5的对象继承是怎么做的？

    参考[ES5如何实现继承（附每种继承方式优缺点）_es5继承的方法-CSDN博客](https://blog.csdn.net/weixin_45844376/article/details/105677231)
    
28. script标签的defer和async的区别是什么？

    defer表示只下载，延迟执行。

    当script中有defer属性时，脚本的加载过程和文档加载是异步发生的，等到文档解析完(DOMContentLoaded事件发生)脚本才开始执行。

    当script有async属性时，脚本的加载过程和文档加载也是异步发生的。但脚本下载完成后会停止HTML解析，执行脚本，脚本解析完继续HTML解析。

    当script同时有async和defer属性时，执行效果和async一致。

30. HTML的Meta标签有什么含义？
        包括一些数据，比如视窗设置、编码格式

29. Object.assign()的使用

    ```javascript
    针对深拷贝，需要使用其他办法，因为 Object.assign()拷贝的是属性值。假如源对象的属性值是一个对象的引用，那么它也只指向那个引用。
    let obj1 = { a: 0 , b: { c: 0}}; 
    let obj2 = Object.assign({}, obj1); 
    console.log(JSON.stringify(obj2)); // { a: 0, b: { c: 0}} 
    
    obj1.a = 1; 
    console.log(JSON.stringify(obj1)); // { a: 1, b: { c: 0}} 
    console.log(JSON.stringify(obj2)); // { a: 0, b: { c: 0}} 
    
    obj2.a = 2; 
    console.log(JSON.stringify(obj1)); // { a: 1, b: { c: 0}} 
    console.log(JSON.stringify(obj2)); // { a: 2, b: { c: 0}}
     
    obj2.b.c = 3; 
    console.log(JSON.stringify(obj1)); // { a: 1, b: { c: 3}} 
    console.log(JSON.stringify(obj2)); // { a: 2, b: { c: 3}} 
    最后一次赋值的时候，b是值是对象的引用，只要修改任意一个，其他的也会受影响
    
    // Deep Clone （深拷贝）
    obj1 = { a: 0 , b: { c: 0}}; 
    let obj3 = JSON.parse(JSON.stringify(obj1)); 
    obj1.a = 4; 
    obj1.b.c = 4; 
    console.log(JSON.stringify(obj3)); // { a: 0, b: { c: 0}}
    ```

30. 

## 408基础

1. 介绍一下计算机网络中的SYN攻击？

   SYN攻击即利用TCP协议缺陷，通过发送大量的半连接请求，占用半连接队列，耗费CPU和内存资源。

   优化方式：

   1. 缩短SYN Timeout时间
   2. 记录IP，若连续受到某个IP的重复SYN报文，从这个IP地址来的包会被一概丢弃。

2. TCP连接中四次挥手是怎么样的？

   1. 第一次挥手：客户端发送一个FIN，用来关闭客户端到服务端的数据传送，客户端进入fin_wait_1状态。
   2. 第二次挥手：服务端收到FIN后，发送一个ACK给客户端，确认序号为收到序号+1，服务端进入Close_wait状态。此时TCP连接处于半关闭状态，即客户端已经没有要发送的数据了，但服务端若发送数据，则客户端仍要接收。
   3. 第三次挥手：服务端发送一个FIN，用来关闭服务端到客户端的数据传送，服务端进入Last_ack状态。
   4. 第四次挥手：客户端收到FIN后，客户端进入Time_wait状态，接着发送一个ACK给服务端，确认后，服务端进入Closed状态，完成四次挥手。

   *追问：为什么要确定四次？*

   主要原因是当服务端收到客户端的 FIN 数据包后，服务端可能还有数据没发完，不会立即close。

   所以服务端会先将 ACK 发过去告诉客户端我收到你的断开请求了，但请再给我一点时间，这段时间用来发送剩下的数据报文，发完之后再将 FIN 包发给客户端表示现在可以断了。之后客户端需要收到 FIN 包后发送 ACK 确认断开信息给服务端。

3. 解释一下Https连接的过程？

   1. 浏览器将支持的加密算法信息发给服务器
   2. 服务器选择一套浏览器支持的加密算法，以证书的形式回发给浏览器
   3. 客户端(SSL/TLS)解析证书验证证书合法性，生成对称加密的密钥，我们将该密钥称之为client key，即客户端密钥，用服务器的公钥对客户端密钥进行非对称加密。
   4. 客户端会发起HTTPS中的第二个HTTP请求，将加密之后的客户端对称密钥发送给服务器
   5. 服务器接收到客户端发来的密文之后，会用自己的私钥对其进行非对称解密，解密之后的明文就是客户端密钥，然后用客户端密钥对数据进行对称加密，这样数据就变成了密文。
   6. 服务器将加密后的密文发送给客户端
   7. 客户端收到服务器发送来的密文，用客户端密钥对其进行对称解密，得到服务器发送的数据。这样HTTPS中的第二个HTTP请求结束，整个HTTPS传输完成。

4. 强制缓存和协商缓存的区别？

   `强制缓存`优先于`协商缓存`，若强制缓存生效则直接使用缓存，若不生效则进行协商缓存，协商缓存由服务器决定是否使用缓存，若协商缓存失效，那么代表该请求的缓存失效，重新获取请求结果，再存入浏览器缓存中；生效则返回304，继续使用缓存。

5. 缓存的位置是在哪？

5. 针对并发请求大量接口的场景，前端如何进行优化？

   1. **合并请求**：将多个接口请求合并成一个请求，减少网络请求次数。可以使用工具如`axios`的并发请求功能或者自定义一个请求合并的逻辑。
   2. **请求分批**：将接口请求分批发送，避免一次性发送大量请求导致网络拥堵。可以设置一个最大并发请求数量，控制同时发送的请求个数。
   3. **接口缓存**：对于一些静态数据或者不经常变化的数据，可以在前端进行缓存，减少对接口的频繁请求。可以使用`localStorage`或者`sessionStorage`进行数据缓存。
   4. **懒加载**：延迟加载部分接口，只有在需要时才发送请求。比如在用户滚动到某个区域或者点击某个按钮时再加载相关接口数据。
   5. **使用CDN**：将静态资源部署到CDN上，提高资源加载速度，减轻服务器压力。
   6. **使用缓存**：利用浏览器缓存机制，合理设置`Cache-Control`、`Expires`等响应头，减少重复请求。
   7. **压缩数据**：对接口返回的数据进行压缩，减少数据传输量，提高网络传输效率。
   8. **使用HTTP/2**：HTTP/2支持多路复用，可以同时在一个连接上发送多个请求和响应，减少网络延迟。

6. 

## 前端工程

1. WebRTC的连接方式有哪些？
   1. 用户 A 和 用户 B 通过 NAT 服务进行映射；
   2. 用户 A 和 用户 B 通过一台中间服务器进行交换各自支持的格式和映射后的 IP + 端口；
   3. 各自得知对方的信息后就可以开始连接通讯。
   
2. vite与webpack的区别
   1. webpack是基于nodejs运行的，但js只能单线程运行，无法利用多核CPU的优势，当项目越来越大时，构建速度也就越来越慢了。
   2. vite预构建与按需编译的过程，都是使用esbuild完成的。esbuild是用go语言编写的，可以充分利用多核CPU的优势，所以vite开发环境下的预构建与按需编译速度，都是非常快的。
   3. webpack项目中，每次修改文件，都会对整个项目重新进行打包，这对大项目来说，是非常不友好的。
   4. 尽管esbuild的打包速度比rollup更快，但 Vite 目前的插件 API 与使用 esbuild 作为打包器并不兼容，rollup插件api与基础建设更加完善，所以在生产环境vite使用rollup打包会更稳定一些。
   
3. cookie和localStorage以及sessionStorage的区别？
   1. cookie：**主要用来保存登录信息**，比如登录某个网站市场可以看到“记住密码”这就是通过cookie中存入一段辨别用户身份的数据来实现的
   2. sessionStorage：会话，是可以将一部分数据在当前的会话中保存下来，**刷新页面数据依旧存在。但是页面关闭时，sessionStorage中的数据就会被清空。**它被存在服务端。
   3. localStorage：是HTML5标准找那个新加入的技术，当然早在IE6时代就有一个userData的东西用于本地存储，当时考虑到浏览器的兼容性，更通用的方案是flash，如今localStorage被大多数浏览器所支持。`localStorage`中的键值对总是**以字符串的形式**存储。`localStorage`类似`sessionStorage`，但其区别在于：存储在`localStorage` 的数据可以长期保留；
   
4. JWT如何进行身份鉴权？
   1.  用户发起登录请求。
   2. 服务器使用私钥创建一个jwt字符串，作为token；
   3. 服务器将这个token返回给浏览器；
   4. 在后续请求中，token作为请求头的内容，发给服务端。
   5. 服务端拿到token之后进行解密，正确解密表示此次请求合法，验证通过；解密失败说明Token无效或者已过期
   6. 返回响应的资源给浏览器
   
5. HTTP请求响应头部字段ETag的用法？

   ETag是URL的tag，用来标示URL对象是否改变。这样可以应用于客户端的缓存：服务器产生ETag，并在HTTP响应头中将其传送到客户端，服务器用它来判断页面是否被修改过，如果未修改返回304，无需传输整个对象。

   ![image.png](question.assets/1588986700552-360c5db2-a839-46c6-9ad2-66b9a2195607.png)

6. 如何减少重绘和回流（重排）？

   重绘：当页面元素样式改变不影响元素在文档流中的位置时（如background-color，border-color，visibility），浏览器只会将新样式赋予元素并进行重新绘制操作。

   回流：当渲染树render tree中的一部分或全部因为元素的规模尺寸、布局、隐藏等改变时，浏览器重新渲染部分DOM或全部DOM的过程

   回流必将引起重绘，而重绘不一定会引起回流。

   CSS方案：

   + 尽可能在DOM树的最末端改变class
   + 避免设置多层内联样式
   + 动画效果应用到position属性为absolute或fixed的元素上
     避免使用table布局
   + 使用css3硬件加速，可以让transform、opacity、filters等动画效果不会引起回流重绘

   JS方案：

   + 避免使用JS一个样式修改完接着改下一个样式，最好一次性更改CSS样式，或者将样式列表定义为class的名称
   + 避免频繁操作DOM，使用文档片段创建一个子树，然后再拷贝到文档中
   + 先隐藏元素，进行修改后再显示该元素，因为display:none上的DOM操作不会引发回流和重绘
   + 避免循环读取offsetLeft等属性，在循环之前把它们存起来
     对于复杂动画效果,使用绝对定位让其脱离文档流，否则会引起父元素及后续元素大量的回流

7. csrf攻击是什么？如何应对？

   CSRF（Cross-Site Request Forgery）跨站请求伪造，是一种利用用户身份在未经许可的情况下以其名义执行非预期操作的攻击方式。黑客可以通过各种方式诱导用户点击特定链接或访问恶意网站，实施CSRF攻击。

   CSRF 攻击的三个条件 :

   1. 用户已经登录了站点 A，并在本地记录了 cookie。

   2. 在用户没有登出站点 A 的情况下（也就是 cookie 生效的情况下），访问了恶意攻击者提供的引诱危险站点 B (B 站点要求访问站点A)。

   3. 站点 A 没有做任何 CSRF 防御。

   防御方法：

   1. **遵循标准的GET动作**只允许GET请求检索数据，但是不允许它修改服务器上的任何数据。
   2. **在非 GET 请求中增加 token并验证**。
   3. **检查 HTTP Referer 字段**同源策略。

8. 如何实现通过refreshToken刷新token呢？

   1. 首先，refreshToken有效时间一定要比token有效时间长至少才能不影响用户体验，具体要长多少得以实际需求为准，建议是24小时
   2. 前端每次访问后端都携带token，如果token失效则后端直接返回类似token失效请重新登录的报文。
   3. 前端第一次收到token失效的响应后，从本地存储拿refreshToken再去请求
   4. 后端一检测到refreshToken参数不为空，就去校验解析这个refreshToken
      1. 如果有效，后端就返回一个新的token及refreshToken给前端，前端收到后更新本地存储，同时拿这个新的token向后端发起第三次请求，然后成功获取资源
      2. 如果无效，则后端同样返回token失效，前端第二次收到失效的响应则跳转到登录页重新登录

9. 浏览器的缓存存放在哪里？

   1.**内存缓存(from memory cache)**：内存缓存具有两个特点，分别是快速读取和时效性：

   快速读取：内存缓存会将编译解析后的文件，直接存入该进程的内存中，占据该进程一定的内存资源，以方便下次运行使用时的快速读取。

   时效性：一旦该进程关闭，则该进程的内存则会清空。

   2.**硬盘缓存(from disk cache)**：硬盘缓存则是直接将缓存写入硬盘文件中，读取缓存需要对该缓存存放的硬盘文件进行I/O操作，然后重新解析该缓存内容，读取复杂，速度比内存缓存慢。退出进程不会清空。

   一般JS,字体，图片等会放在内存中，而CSS则会放在硬盘缓存中

9. Content-Type是什么？

   Content-type 定义了 http 请求的数据类型。

   如果设置在请求头中，则定义的是请求体的数据类型；

   如果设置在响应头中，则定义的是响应体的数据类型；

   请求头--Request-Headers：一般我们在 post 请求中会向服务器发送一些参数，那我们就可以通过这个参数设置 post 的参数格式

   + application/json：JSON 数据格式；
   + application/x-www-form-urlencoded：表单默认的提数据格式；
   + multipart/form-data：一般用于文件上传；

   响应头--Response-Headers：响应头里的 Content-Type 参数会告诉我们响应数据的格式，一般我们可以在请求头里面设置我们想要的数据格式（Accept）

10. Object.create(null) 和 {} 区别

    参考[Object.create(null) 和 {} 区别 - 孟繁贵 - 博客园 (cnblogs.com)](https://www.cnblogs.com/mengfangui/p/9767125.html#:~:text=Object.create (null) 和 {} 区别 Object.create,(null) 创建一个空对象，此对象无原型方法。 {} 其实是new Object ()，具有原型方法。)

    Object.create(null) 创建一个空对象，此对象无原型方法。

    {} 其实是new Object()，具有原型方法。
    
11. Qiankun是怎么实现JS隔离的？

    [Qiankun 原理-js沙箱是怎么做隔离的 - 知乎 (zhihu.com)](https://zhuanlan.zhihu.com/p/578093950)

    它有三个沙箱模式，分别是**SnapshotSandbox 快照沙箱**、**legacySandbox 单例沙箱**、**proxySandbox 多例沙箱**。

    快照沙箱就是,给你拍一张照片，记录你此时的状态，乾坤的快照沙箱是根据diff 来实现的，主要用于不支持window.Proxy的低版本浏览器。激活沙箱时，将window的快照信息存在windowSnapshot中，如果modifyPropsMap有值，还需要还原上次的状态，激活期间，可能修改了window的数据，退出沙箱，将修改过的信息存到modifyPropsMap 里面，并且把window ，还原初始进入的状态。

    SnapshotSandbox 的缺点在于：

    1. 需要遍历window上的所有属性，性能差
    2. 同时间内只能激活一个微应用

    legacySandbox 设置三个参数来记录全局变量，分别记录沙箱新增的全局变量addedPropsMapInSandbox，记录沙箱更新的全局变量modifiedPropsOriginalValueMapInSandbox，用于在任意时刻做snapshot的currentUpdatePropsValueMap。

    ProxySandbox的原理在于激活沙箱后，每次对window 取值的时候，先从自己沙箱环境的fakeWindow 里面找，如果不存在，就从`rawWindow`(外部的`window`)里去找；当对沙箱内部window 对象赋值的时候，会直接操作fakeWindow，而不会影响到rawWindow。

12. 事件冒泡和事件捕获是什么？

    [你真的理解事件绑定、事件冒泡和事件委托吗？-阿里云开发者社区 (aliyun.com)](https://developer.aliyun.com/article/897883)

    事件冒泡是从子组件到父组件；事件捕获是从父组件到子组件。

13. 为什么要涉及堆内存与栈内存？

    [详解JS中的栈内存与堆内存！（配图解）_堆内存和栈内存图解-CSDN博客](https://blog.csdn.net/qq_45850095/article/details/117922294)

14. HTTPS通讯流程

    1、客户端向服务端发起建立HTTPS请求。

    2、服务器向客户端发送数字证书。

    3、客户端验证数字证书，证书验证通过后客户端生成会话密钥（双向验证则此处客户端也会向服务器发送证书）。

    4、服务器生成会话密钥（双向验证此处服务端也会对客户端的证书验证）。

    5、客户端与服务端开始进行加密会话。

15. 浏览器渲染HTML的步骤

    1. 浏览器接收到一个html文档，渲染引擎会立即解析它，并将其html元素生成对应的DOM节点，组成一个”DOM树“。
    2. 浏览器解析来自CSS文件和html中内联的样式，然后根据这些样式信息生成css对象模型树(CSSOM tree),接着和上一步创建的DOM树合并为一个“渲染树“。
    3. 浏览器引擎根据渲染树计算出每个节点在其屏幕上应该出现的精确位置，并分配这组坐标，这样的过程称为“布局”，也称为“自动重排”。
    4. 浏览器遍历渲染树，调用每一个节点的paint方法来绘制这些渲染对象，通过绘制内容，最终在屏幕上展示内容。该过程称为”绘制“或者”栅格化“。

16. JSONP的具体流程

    1、首先是利用script标签的src属性来实现跨域。
    2、通过将前端方法作为参数传递到服务器端，然后由服务器端注入参数之后再返回，实现服务器端向客户端通信。
    3、由于使用script标签的src属性，因此只支持get方法

17. HTTP 2.0有哪些新特性？

    HTTP 2.0提供了：二进制分帧、首部压缩、多路复用、请求优先级、服务器推送等优化。

18. websocket和长轮询区别？

    Websockets：提供全双工通信，即客户端和服务器可以同时发送和接收信息。一旦建立连接，信息可以随时在双方之间传递。

    HTTP长轮询：工作在传统的HTTP协议之上，通过客户端不断发送请求给服务器来获取最新数据。服务器在有数据更新时才响应请求。

    Websockets：不所有的网络环境都支持Websockets，可能会被某些代理和防火墙阻塞。

    HTTP长轮询：兼容性较好，因为它基于标准的HTTP协议。

19. WebSocket与Socket、TCP、HTTP的关系及区别？

    IP：网络层协议；（高速公路）

    TCP和UDP：传输层协议；（卡车）

    HTTP：应用层协议；（货物）。HTTP(超文本传输协议)是建立在TCP协议之上的一种应用。HTTP连接最显著的特点是客户端发送的每次请求都需要服务器回送响应，在请求结束后，会主动释放连接。从建立连接到关闭连接的过程称为“一次连接”。

    SOCKET：套接字，TCP/IP网络的API。(港口码头/车站)Socket是应用层与TCP/IP协议族通信的中间软件抽象层，它是一组接口。socket是在应用层和传输层之间的一个抽象层，它把TCP/IP层复杂的操作抽象为几个简单的接口供应用层调用已实现进程在网络中通信。

    Websocket：同HTTP一样也是应用层的协议，但是它是一种双向通信协议，是建立在TCP之上的，解决了服务器与客户端全双工通信的问题，包含两部分:一部分是“握手”，一部分是“数据传输”。握手成功后，数据就直接从 TCP 通道传输，与 HTTP 无关了。

20. http的keep-alive和tcp的keepalive区别？

    http keep-alive是为了让tcp活得更久一点，以便在同一个连接上传送多个http，提高socket的效率。而tcp keep-alive是TCP的一种检测TCP连接状况的保鲜机制。

21. XSS 与 CSRF 攻击有什么区别？

    [XSS 与 CSRF 攻击——有什么区别? - FreeBuf网络安全行业门户](https://www.freebuf.com/vuls/339009.html)

22. DNS的查询流程?

    - 首先搜索浏览器的 DNS 缓存，缓存中维护一张域名与 IP 地址的对应表

    - 若没有命中，则继续搜索操作系统的 DNS 缓存
    
    - 若仍然没有命中，则操作系统将域名发送至本地域名服务器，本地域名服务器采用递归查询自己的 DNS 缓存，查找成功则返回结果
    
    - 若本地域名服务器的 DNS 缓存没有命中，则本地域名服务器向上级域名服务器进行迭代查询
    
    - - 首先本地域名服务器向根域名服务器发起请求，根域名服务器返回顶级域名服务器的地址给本地服务器
      - 本地域名服务器拿到这个顶级域名服务器的地址后，就向其发起请求，获取权限域名服务器的地址
      - 本地域名服务器根据权限域名服务器的地址向其发起请求，最终得到该域名对应的 IP 地址
    
    - 本地域名服务器将得到的 IP 地址返回给操作系统，同时自己将 IP 地址缓存起来
    
    - 操作系统将 IP 地址返回给浏览器，同时自己也将 IP 地址缓存起
    
    - 至此，浏览器就得到了域名对应的 IP 地址，并将 IP 地址缓存起
    
23. http2.0解决了1.0和1.1哪些问题?

    1. 多路复用：HTTP2.0引入了多路复用的概念，允许在单一连接上同时传输多个请求和响应，减少了连接的开销和延迟。这使得网页加载更快，提高了用户体验。
    2. 头部压缩：HTTP2.0采用HPACK算法对头部信息进行压缩，减少了传输的数据量，提高了传输效率。
    3. 服务器推送：HTTP2.0允许服务器主动向客户端推送资源，减少了客户端请求的次数和响应时间。
    4. 流量控制：HTTP2.0引入了流量控制机制，允许客户端和服务器分别控制数据传输的速度，避免了因网络拥堵导致的数据丢失或延迟。
    5. 更好的安全性：HTTP2.0支持TLS加密，提供了更好的安全性。通过使用TLS加密，可以保护数据在传输过程中的隐私和完整性。

24. XSS防御手段有哪些？

    从输入到输出都需要过滤、转义。

25. js中的bom和dom的区别?
    
    BOM是浏览器对象模型
    提供了独立于内容而与浏览器窗口进行交互的对象。描述了与浏览器进行交互的方法和接口，可以对浏览器窗口进行访问和操作，譬如可以弹出新的窗口，改变状态栏中的文本，对Cookie的支持，IE还扩展了BOM，加入了ActiveXObject类，可以通过js脚本实例化ActiveX对象等等）
    
    DOM是文档对象模型
    DOM是针对XML的基于树的API。描述了处理网页内容的方法和接口，是HTML和XML的API，DOM把整个页面规划成由节点层级构成的文档。DOM本身是与语言无关的API，它并不与Java，JavaScript或其他语言绑定。
    
    1、BOM的最根本对象是window。
    
    2、DOM最根本对象是document（实际上是window.document）。
    
26. 怎么记录白屏时间？

    可用Preformance API时： 白屏时间 = firstPaint - performance.timing.navigationStart
    不可用Preformance API时：白屏时间 = firstPaint - pageStartTime

27. HTTPS包篡改怎么保证安全性？

    参考[【面试必备】https到底是如何防篡改的？ - 知乎 (zhihu.com)](https://zhuanlan.zhihu.com/p/269827927)

28. 前端优化的指标有哪些？

29. 如何优化three.js的渲染性能？

    优化Three.js的渲染性能可以通过以下方法实现：减少绘制调用次数、使用贴图合并、减少着色器切换、优化几何体、使用视锥体裁剪等。此外，还可以通过使用Web Workers来将计算密集型任务移至后台线程，避免阻塞主线程。

30. options请求是什么？

参考[科普一下 CORS 以及如何节省一次 OPTIONS 请求 - 知乎 (zhihu.com)](https://zhuanlan.zhihu.com/p/70032617)

31. http3.0带来了什么？

    基于QUIC协议；加密通信；连接迁移

32. webworker有什么用，如何通信？

    WebWorker 可以让 Web 应用利用多核 CPU 的并行计算能力，充分发挥计算机硬件性能。它允许[开发者](https://cloud.baidu.com/product/xly.html)在主线程之外，创建新的工作线程，这些线程可以独立于主线程运行，不会阻塞用户界面。工作线程与主线程之间的通信是通过[消息](https://cloud.baidu.com/product/kafka.html)传递实现的，保证了两者之间的[数据安全](https://cloud.baidu.com/solution/security/datacirculation.html)和同步。

33. spa是什么？

34. 


## Javascript

1. 原生JS如何绑定事件？

   ```javascript
   document.querySelector(".div2").addEventListener("click", () => { 
       console.log('DIV 2');
     },true);
   
   document.querySelector(".div2").addEventListener("click", () => { 
       console.log('DIV 2');
     }); // 已注册为冒泡
   ```

2. 手写instanceof

   ```javascript
   function myInstanceOf(obj, constructor) {
     let proto = Object.getPrototypeOf(obj);
   
     while (proto !== null) {
       if (proto === constructor.prototype) {
         return true;
       }
       proto = Object.getPrototypeOf(proto);
     }
   
     return false;
   }
   
   // 示例用法
   function Person(name) {
     this.name = name;
   }
   
   let person = new Person("Alice");
   console.log(myInstanceOf(person, Person)); // true
   console.log(myInstanceOf(person, Object)); // true
   console.log(myInstanceOf(person, Array)); // false
   
   ```

3. js获取dom元素的css 

   ```javascript
   js获取dom元素的css
   要获取DOM元素的CSS样式，可以使用JavaScript中的getComputedStyle方法。以下是一个示例代码：
   
   // 获取id为elementId的DOM元素
   var element = document.getElementById('elementId');
   
   // 获取元素的CSS样式
   var style = window.getComputedStyle(element);
   
   // 获取特定CSS属性的值
   var color = style.getPropertyValue('color');
   var fontSize = style.getPropertyValue('font-size');
   
   console.log('Color:', color);
   console.log('Font Size:', fontSize);
   在这个示例中，我们首先获取了一个ID为elementId的DOM元素，然后使用getComputedStyle方法获取了该元素的CSS样式。最后，我们可以通过getPropertyValue方法获取特定CSS属性的值，例如color和font-size。
   ```

4. 实现观察者模式EventBus

   [面试官：请手写一个EventBus，让我看看你的代码能力！ - 掘金 (juejin.cn)](https://juejin.cn/post/7101481154565865486)

   ```javascript
     class EventBus {
       // 定义所有事件列表,此时需要修改格式：
       // // {
       //   key: {
       //     id: Function,
       //     id: Function
       //   },
       //   key: Object,
       // } 
       // Array存储的是注册的回调函数
       constructor() {
         this.eventObj = {}; // 用于存储所有订阅事件
         this.callbcakId = 0; // 每个函数的ID
       }
       // 订阅事件,类似监听事件$on('key',()=>{})
       $on(name, callbcak) {
         // 判断是否存储过
         if (!this.eventObj[name]) {
           this.eventObj[name] = {};
         }
         // 定义当前回调函数id
         const id = this.callbcakId++;
         this.eventObj[name][id] = callbcak; // 以键值对的形式存储回调函数
         return id; // 将id返回出去，可以利用该id取消订阅
       }
       // 发布事件,类似于触发事件$emit('key')
       $emit(name, ...args) {
         // 获取存储的事件回调函数数组
         const eventList = this.eventObj[name];
         // 执行所有回调函数且传入参数
         for (const id in eventList) {
           eventList[id](...args);
         }
       }
       // 取消订阅函数，类似于$off('key1', id)
       $off(name, id) {
         // 删除存储在事件列表中的该事件
         delete this.eventObj[name][id];
         console.info(`id为${id}的事件已被取消订阅`)
         // 如果这是最后一个订阅者，则删除整个对象
         if (!Object.keys(this.eventObj[name]).length) {
           delete this.eventObj[name];
         }
       }
     }
     // 初始化EventBus
     let EB = new EventBus();
   
   
     // 订阅事件
     EB.$on('key1', (name, age) => {
       console.info("我是订阅事件A:", name, age);
     })
     let id = EB.$on("key1", (name, age) => {
       console.info("我是订阅事件B:", name, age);
     })
     EB.$on("key2", (name) => {
       console.info("我是订阅事件C:", name);
     })
   
   
     // 发布事件key1
     EB.$emit('key1', "小猪课堂", 26);
     // 取消订阅事件
     EB.$off('key1', id);
     // 发布事件key1
     EB.$emit('key1', "小猪课堂", 26);
     // 发布事件
     EB.$emit('key2', "小猪课堂");
   ```

5. 手写AJAX

   [面试题：手写ajax - 掘金 (juejin.cn)](https://juejin.cn/post/6992604888165253156)

   ```javascript
   const getJSON = function (url) {
     return new Promise((resolve, reject) => {
       const xhr = new XMLHttpRequest();
       xhr.open("GET", url, false);
       xhr.setRequestHeader("Content-Type", "application/json");
       xhr.onreadystatechange = function () {
         if (xhr.readyState !== 4) return;
         if (xhr.status === 200 || xhr.status === 304) {
           resolve(xhr.responseText);
         } else {
           reject(new Error(xhr.responseText));
         }
       };
       xhr.send();
     });
   };
   
   ```

6. 手写深度拷贝

   [javascript - 前端手写代码系列文章(一): 手写深克隆方法 - 个人文章 - SegmentFault 思否](https://segmentfault.com/a/1190000041927503)

   ```javascript
   function deepClone(target){
   
     if(typeof target !== 'object'){
       return target;
     }
     
     const cloneTarget = Array.isArray(target) ? [] : {};
     
     for(let key in target){
       cloneTarget[key] = deepClone(target[key]);
     }
     
     return cloneTarget;
     
   }
   ```

7. 手写数组变成二叉搜索树

   ```javascript
   // 定义节点类
   class TreeNode {
     constructor(val, left = null, right = null) {
       this.val = (val === undefined ? 0 : val);
       this.left = (left === undefined ? null : left);
       this.right = (right === undefined ? null : right);
     }
   }
   
   // 将有序数组转为平衡二叉搜索树
   function sortedArrayToBST(nums) {
     if (!nums.length) return null;
     
     // 找到中间元素作为根节点
     let midIndex = Math.floor(nums.length / 2);
     let root = new TreeNode(nums[midIndex]);
   
     // 递归构建左子树和右子树
     root.left = sortedArrayToBST(nums.slice(0, midIndex));
     root.right = sortedArrayToBST(nums.slice(midIndex + 1));
   
     return root;
   }
   
   // 示例
   let nums = [-10, -3, 0, 5, 9];
   let bst = sortedArrayToBST(nums);
   ```

8. 合成一个N*N，并且元素全为0的二维数组

   ```javascript
   Array.from({ length: size }, () => Array(size).fill(0)
   ```

9. 手写promise.all

   ```javascript
   function PromiseAll(promiseArray) {    //返回一个Promise对象
        return new Promise((resolve, reject) => {
        
           if (!Array.isArray(promiseArray)) {                        //传入的参数是否为数组
               return reject(new Error('传入的参数不是数组！'))
           }
   
           const res = []
           let counter = 0                         //设置一个计数器
           for (let i = 0; i < promiseArray.length; i++) {
               Promise.resolve(promiseArray[i]).then(value => {
                   counter++                  //使用计数器返回 必须使用counter
                   res[i] = value
                   if (counter === promiseArray.length) {
                       resolve(res)
                   }
               }).catch(e => reject(e))
           }
       })
   }
   
   ```

10. 手写一个Promise.race

   ```javascript
   function race(arr){
     return new Promise((res,rej) => {
       for(let i = 0; i < arr.length; i++){
         arr[i].then(resolve => {
           res(resolve)  //某一promise完成后直接返回其值
         }).catch(e => {
           rej(e)  //如果有错误则直接结束循环，并返回错误
         })
       }
     })
   }
   ```

10. ```shell
    实现一个 HardMan:
    ```

    HardMan("jack") 输出:
     I am jack

     HardMan("jack").rest(10).learn("computer") 输出
     I am jack
     //等待10秒
     Start learning after 10 seconds
     Learning computer

     HardMan("jack").restFirst(5).learn("chinese") 输出
     //等待5秒
     Start learning after 5 seconds
     I am jack

    Learning chinese

    ```javascript
    function hardMan(str) {
        this.queue = []
        this.name = str
        this.queue.push(() => {
            console.log(this.name);
            this.next()
        });
    }
    
    hardMan.prototype.rest = function (wait) {
        const func = () => {
            setTimeout(() => {
                console.log(`Start learning after ${wait} seconds`)
                this.next()
            }, wait * 1000)
        }
        this.queue.push(func)
        return this
    }
    
    hardMan.prototype.restFirst = function (wait) {
        const func = () => {
            setTimeout(() => {
                console.log(`Start learning after ${wait} seconds`)
                this.next()
            }, wait * 1000)
        }
        this.queue.unshift(func)
        return this
    }
    
    hardMan.prototype.learn = function (str) {
        const func = () => {
            console.log(`Learning ${str}`)
        }
        this.queue.push(func)
        this.next()
    }
    hardMan.prototype.next = function () {
        if (this.queue.length === 0) return
        const func = this.queue.shift()
        func()
    }
    const HardMan = (name)=>{
        return new hardMan("jack");
    } 
    ```

11. 判断两个对象是否相等

    ```javascript
      function isObjectValueEqualNew(a, b) {
        var aProps = Object.getOwnPropertyNames(a);
        var bProps = Object.getOwnPropertyNames(b);
        if (aProps.length != bProps.length) {
          return false;
        }
        for (var i = 0; i < aProps.length; i++) {
          var propName = aProps[i]
    
          var propA = a[propName]
          var propB = b[propName]
          if ((typeof (propA) === 'object')) {
            if (this.isObjectValueEqualNew(propA, propB)) {
              return true
            } else {
              return false
            }
          } else if (propA !== propB) {
            return false
          } else {
            //
          }
        }
        return true
      }
    ```

12. 手写K并发

    ```javascript
    function multiRequest(urls = [], maxNum) {
      // 请求总数量
      const len = urls.length;
      // 根据请求数量创建一个数组来保存请求的结果
      const result = new Array(len).fill(false);
      // 当前完成的数量
      let count = 0;
    
      return new Promise((resolve, reject) => {
        // 请求maxNum个
        while (count < maxNum) {
          next();
        }
        function next() {
          let current = count++;
          // 处理边界条件
          if (current >= len) {
            // 请求全部完成就将promise置为成功状态, 然后将result作为promise值返回
            !result.includes(false) && resolve(result);
            return;
          }
          const url = urls[current];
          console.log(`开始 ${current}`, new Date().toLocaleString());
          fetch(url)
            .then((res) => {
              // 保存请求结果
              result[current] = res;
              console.log(`完成 ${current}`, new Date().toLocaleString());
              // 请求没有全部完成, 就递归
              if (current < len) {
                next();
              }
            })
            .catch((err) => {
              console.log(`结束 ${current}`, new Date().toLocaleString());
              result[current] = err;
              // 请求没有全部完成, 就递归
              if (current < len) {
                next();
              }
            });
        }
      });
    }
    ```

    第二个解决方法

    ```javascript
    // 并发请求函数
    const concurrencyRequest = (urls, maxNum) => {
        return new Promise((resolve) => {
            if (urls.length === 0) {
                resolve([]);
                return;
            }
            const results = [];
            let index = 0; // 下一个请求的下标
            let count = 0; // 当前请求完成的数量
    
            // 发送请求
            async function request() {
                if (index === urls.length) return;
                const i = index; // 保存序号，使result和urls相对应
                const url = urls[index];
                index++;
                console.log(url);
                try {
                    const resp = await fetch(url);
                    // resp 加入到results
                    results[i] = resp;
                } catch (err) {
                    // err 加入到results
                    results[i] = err;
                } finally {
                    count++;
                    // 判断是否所有的请求都已完成
                    if (count === urls.length) {
                        console.log('完成了');
                        resolve(results);
                    }
                    request();
                }
            }
    
            // maxNum和urls.length取最小进行调用
            const times = Math.min(maxNum, urls.length);
            for(let i = 0; i < times; i++) {
                request();
            }
        })
    }
    

第三个解决方法

```javascript
let requestList = [];
for (let i = 1; i <= 100; i++) {
  requestList.push(
    () =>
      new Promise((resolve, reject) => {
        setTimeout(
          () => {
            if (i % 10 === 2) {
              reject(new Error("出错了，出错请求：" + i));
            } else {
              console.log("done", i);
              resolve(i);
            }
          },
          i % 10 === 3 ? Math.random() * 1000 : Math.random() * 5000 + 5000,
        );
      }),
  );
}


// 运行池
const pool = new Set();

// 等待队列
const waitQueue = [];
const request = (reqFn, max) => {
  return new Promise((resolve, reject) => {
    const isFull = pool.size >= max;
    const newReqFn = () => {
      reqFn()
        .then((res) => resolve(res))
        .catch((err) => reject(err))
        .finally(() => {
          pool.delete(newReqFn);
          const next = waitQueue.shift();
          if (next) {
            pool.add(next);
            next();
          }
        });
    };
    if (isFull) {
      waitQueue.push(newReqFn);
    } else {
      pool.add(newReqFn);
      newReqFn();
    }
  });
};

requestList.forEach(async (item) => {
  // const res = await request(item, 10);
  // console.log(res);
  request(item, 10)
    .then((res) => console.log(res))
    .catch((err) => console.log(err));
});

```

