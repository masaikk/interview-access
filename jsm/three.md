# ThreeJs

## mesh

### 展示一个mesh模型

创建这种模型，需要考虑位置参数以及材质

```typescript
      const geometry = new THREE.BoxGeometry(30, 20, 30);
      const material = new THREE.MeshPhongMaterial({ color: 0xffffff });
      const cube = new THREE.Mesh(geometry, material);
```

下面是vue3的demo代码

```vue
<template>
  <div ref="container"></div>
</template>

<script setup>
import * as THREE from "three";
import { onMounted, ref } from "vue";

const container = ref(null);
let scene, camera, renderer;

onMounted(() => {
  // 初始化渲染
  const width = window.innerWidth;
  const height = window.innerHeight;

  scene = new THREE.Scene();
  camera = new THREE.PerspectiveCamera(75, width / height, 0.1, 1000);
  renderer = new THREE.WebGLRenderer();

  renderer.setSize(width, height);
  container.value.appendChild(renderer.domElement);

  // 创建几何体
  const geometry = new THREE.BoxGeometry(1, 1, 1);
  const material = new THREE.MeshBasicMaterial({
    color: 0x00ff00,
    wireframe: true,
  });
  const cube = new THREE.Mesh(geometry, material);

  // 添加到场景中
  scene.add(cube);

  // 设置相机的位置和朝向
  camera.position.z = 5;

  // 动画循环
  function animate() {
    requestAnimationFrame(animate);

    // 在这里进行模型的旋转、平移等操作
    cube.rotation.x += 0.01;
    cube.rotation.y += 0.01;

    renderer.render(scene, camera);
  }

  animate();
});
</script>

<style scoped>
#container {
  width: 100%;
  height: 100%;
}
</style>

```

参考官方文档[BufferGeometry – three.js docs (threejs.org)](https://threejs.org/docs/index.html?q=BufferGeometry#api/en/core/BufferGeometry)

可以使用节点的列表以及连接节点的数组来构建集合体。如下所示

```vue
<template>
  <div ref="container" id="container"></div>
</template>

<script setup>
import * as THREE from "three";
import { onMounted, ref } from "vue";

const container = ref(null);
let scene, camera, renderer;

onMounted(() => {
  // 初始化渲染
  const width = window.innerWidth;
  const height = window.innerHeight;

  scene = new THREE.Scene();
  camera = new THREE.PerspectiveCamera(75, width / height, 0.1, 1000);
  renderer = new THREE.WebGLRenderer({ alpha: true });

  renderer.setSize(width, height);
  container.value.appendChild(renderer.domElement);

  // 自定义顶点数据
  const vertices = [
    -1,
    -1,
    -1 / Math.sqrt(3), // 底面三角形的左下角顶点
    1,
    -1,
    -1 / Math.sqrt(3), // 底面三角形的右下角顶点
    0,
    1,
    -1 / (2 * Math.sqrt(3)), // 顶部顶点
    0,
    0,
    1 / (2 * Math.sqrt(3)), // 底面中心顶点
  ];

  // 如果要按照逆时针顺序连接顶点，可以定义一个顶点索引数组
  const indices = [0, 1, 2, 0, 2, 3, 1, 2, 3];

  // 创建BufferGeometry对象
  const geometry = new THREE.BufferGeometry();

  // 设置顶点属性
  const positionAttribute = new THREE.Float32BufferAttribute(vertices, 3);
  geometry.setAttribute("position", positionAttribute);

  // 设置顶点索引
  geometry.setIndex(indices);

  const material = new THREE.MeshBasicMaterial({
    color: 0x00ff00,
    wireframe: true,
  });
  const cube = new THREE.Mesh(geometry, material);

  // 添加到场景中
  scene.add(cube);

  // 设置相机的位置和朝向
  camera.position.z = 5;

  // 动画循环
  function animate() {
    requestAnimationFrame(animate);

    // 在这里进行模型的旋转、平移等操作
    cube.rotation.x += 0.01;
    cube.rotation.y += 0.01;

    renderer.render(scene, camera);
  }

  animate();
});
</script>
```

### 摄像机

```javascript
const width = window.innerWidth;
const height = window.innerHeight;
const camera = new THREE.PerspectiveCamera(45, width / height, 0.1, 1000);
```

可以展示一个摄像机

![img](three.assets/fc96a1141fcc4780914ff01366360774tplv-k3u1fbpfcp-zoom-in-crop-mark1512000.awebp)

从一个位置去看，是不是得指定你看的角度是多大，也就是图里的 fov，上面指定的 45 度。

然后就是你看的这个范围的宽高比是多少，我们用的是窗口的宽高比。

再就是你要看从哪里到哪里的范围，我们是看从 0.1 到距离 1000 的范围。

## MMD

参考项目[threeDemo: 使用threejs的demo (gitee.com)](https://gitee.com/masaikk/three-demo)

如果想在mmd项目中添加物理依赖，可以初始化ammo.js。在导入文件后，可以考虑如下代码插入到index.html中。

```html
    <script src="/ammo.js">
      Ammo().then(function(AmmoLib)
      {
        Ammo=AmmoLib;
        init();animate();
      })
    </script>
```

我们可以监听键盘的上下左右的按键动作，来控制camera位置

```javascript
const useKeyboardEvent = () => {
  const handleKeyDown = (event) => {
    switch (event.key) {
      case "s": {
        cameraPosition.y += 10;
        break;
      }
      case "w": {
        cameraPosition.y -= 10;
        break;
      }
      case "a": {
        cameraPosition.x += 10;
        break;
      }
      case "d": {
        cameraPosition.x -= 10;
        break;
      }
    }
    camera.position.set(cameraPosition.x, cameraPosition.y, cameraPosition.z);
  };

  const handleKeyUp = (event) => {
    // console.log(`Key up: ${event.key}`);
  };

  onMounted(() => {
    window.addEventListener("keydown", handleKeyDown);
    window.addEventListener("keyup", handleKeyUp);
  });

  onUnmounted(() => {
    window.removeEventListener("keydown", handleKeyDown);
    window.removeEventListener("keyup", handleKeyUp);
  });
};

```

在循环的render中渲染摄像机对象

```javascript
  // 动画循环
  function animate() {
    requestAnimationFrame(animate);
    renderer.render(scene, camera);
  }

  animate();
```

我们可以加载模型并且使用官方给定的套件

```vue
<template>
  <div>
    <div ref="container" id="container"></div>
  </div>
</template>

<script setup>
import { MMDAnimationHelper } from "three/examples/jsm/animation/MMDAnimationHelper.js";
import { MMDLoader } from "three/examples/jsm/loaders/MMDLoader.js";
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls";
import * as THREE from "three";
import { onMounted, reactive, ref } from "vue";

const container = ref(null);

let scene, camera, renderer, clock;
let frameId;

onMounted(() => {
  // 相机位置的响应式数据
  const cameraPosition = reactive({
    x: 0,
    y: 0,
    z: 75,
  });
  const width = window.innerWidth;
  const height = window.innerHeight;
  scene = new THREE.Scene();
  camera = new THREE.PerspectiveCamera(750, width / height, 0.1, 2000);
  const loader = new MMDLoader();
  const light = new THREE.AmbientLight(0xa0a0a0);
  scene.add(light);

  camera.position.set(cameraPosition.x, cameraPosition.y, cameraPosition.z);

  const helper = new MMDAnimationHelper();
  const animations = []; // 存储加载的所有vmd动画

  loader.loadWithAnimation(
    "/qque/青雀（分桦）.pmx",
    "/move/tw.vmd",
    function onLoad(mmd) {
      // const { mesh } = mmd;
      helper.add(mmd.mesh, {
        animation: mmd.animation,
        physics: true,
      });

      scene.add(mmd.mesh);
    },
    undefined,
    undefined
  );

  renderer = new THREE.WebGLRenderer({ antialias: true, alpha: false });
  renderer.setSize(width, height);
  renderer.setClearColor(new THREE.Color(0x000000));
  clock = new THREE.Clock();

  container.value.appendChild(renderer.domElement);

  const controls = new OrbitControls(camera, renderer.domElement);
  controls.target.set(0, 10, 0);
  controls.update();

  function render() {
    helper.update(clock.getDelta());
    renderer.render(scene, camera);
    requestAnimationFrame(render);
  }

  controls.addEventListener("change", render);
  render();
});
</script>

```

下面是一个展示的demo

![image-20230916144745923](three.assets/image-20230916144745923.png)

---

## 跳一跳demo

参考这个链接

[Three.js 手写跳一跳小游戏（上） - 掘金 (juejin.cn)](https://juejin.cn/post/7269640045044613160)

在这里游戏里面，可以考虑一下点光源以及线光源的区别。

```vue
<template>
  <div></div>
</template>

<script lang="ts" setup>
import * as THREE from "three";
import { onMounted, ref } from "vue";

onMounted(() => {
  const width = window.innerWidth;
  const height = window.innerHeight;
  const camera = new THREE.PerspectiveCamera(45, width / height, 0.1, 1000);

  const scene = new THREE.Scene();
  const renderer = new THREE.WebGLRenderer();

  renderer.setSize(width, height);
  renderer.setClearColor(0x333333);
  camera.position.set(100, 100, 100);
  camera.lookAt(scene.position);

  const directionalLight = new THREE.DirectionalLight(0xffffff);
  directionalLight.position.set(40, 100, 60);

  scene.add(directionalLight);

  const axesHelper = new THREE.AxesHelper(1000);
  axesHelper.position.set(0, 0, 0);
  scene.add(axesHelper);

  document.body.appendChild(renderer.domElement);

  function create() {
    const geometry = new THREE.BoxGeometry(30, 20, 30);
    const material = new THREE.MeshPhongMaterial({ color: 0xffffff });
    const cube = new THREE.Mesh(geometry, material);
    scene.add(cube);

    const geometry2 = new THREE.BoxGeometry(30, 20, 30);
    const material2 = new THREE.MeshPhongMaterial({ color: 0xffffff });
    const cube2 = new THREE.Mesh(geometry2, material2);
    cube2.position.z = -50;
    scene.add(cube2);
  }

  function render() {
    renderer.render(scene, camera);
    requestAnimationFrame(render);
  }

  create();
  render();
});
</script>

<style scoped></style>

```

以上的代码可以显示出这种效果：

![image-20230916161800960](three.assets/image-20230916161800960.png)

考虑到边缘出现了锯齿情况，可以考虑修改renderer配置

```javascript
const renderer = new THREE.WebGLRenderer({ antialias: true });
```

抗锯齿效果很明显，如下所示：

![image-20230916162104475](three.assets/image-20230916162104475.png)

使用如下的代码，添加玩家，以及点击事件

```typescript
  function createPlayer() {
    const geometry = new THREE.BoxGeometry(5, 20, 5);
    const material = new THREE.MeshPhongMaterial({ color: 0x000000 });
    const player = new THREE.Mesh(geometry, material);
    player.position.x = 0;
    player.position.y = 17.5;
    player.position.z = 0;
    scene.add(player);
    return player;
  }

  const player = createPlayer();

  document.body.addEventListener("click", () => {
    player.position.z -= 100;
  });
```

![image-20230916163217564](three.assets/image-20230916163217564.png)

很显然，这样跳两下，人物就出去了，所以，需要修改摄像头的位置。在跳动的时候修改位置

```javascript
  let focusPos = { x: 0, y: 0, z: 0 };
  document.body.addEventListener("click", () => {
    player.position.z -= 100;

    camera.position.z -= 100;

    focusPos.z -= 100;
    camera.lookAt(focusPos.x, focusPos.y, focusPos.z);
  });
```

现在还可以添加动作的流畅

![image-20230916170414507](three.assets/image-20230916170414507.png)

```vue
<template>
  <div></div>
</template>

<script lang="ts" setup>
import * as THREE from "three";
import { onMounted, ref } from "vue";

onMounted(() => {
  const width = window.innerWidth;
  const height = window.innerHeight;
  const camera = new THREE.PerspectiveCamera(90, width / height, 0.1, 1000);

  const scene = new THREE.Scene();
  const renderer = new THREE.WebGLRenderer({ antialias: true });

  renderer.setSize(width, height);
  renderer.setClearColor(0x333333);
  camera.position.set(100, 100, 100);
  camera.lookAt(scene.position);

  const directionalLight = new THREE.DirectionalLight(0xffffff);
  directionalLight.position.set(40, 100, 60);

  scene.add(directionalLight);

  const axesHelper = new THREE.AxesHelper(1000);
  axesHelper.position.set(0, 0, 0);
  scene.add(axesHelper);

  document.body.appendChild(renderer.domElement);

  function create() {
    function createCube(x: number, z: number) {
      const geometry = new THREE.BoxGeometry(30, 20, 30);
      const material = new THREE.MeshPhongMaterial({ color: 0xffffff });
      const cube = new THREE.Mesh(geometry, material);
      cube.position.x = x;
      cube.position.z = z;
      scene.add(cube);
    }

    createCube(0, 0);
    createCube(0, -100);
    createCube(0, -200);
    createCube(0, -300);
    createCube(-100, 0);
    createCube(-200, 0);
    createCube(-300, 0);
  }

  function render() {
    moveCamera();
    movePlayer();

    renderer.render(scene, camera);
    requestAnimationFrame(render);
  }

  function createPlayer() {
    const geometry = new THREE.BoxGeometry(5, 20, 5);
    const material = new THREE.MeshPhongMaterial({ color: 0x000000 });
    const player = new THREE.Mesh(geometry, material);
    player.position.x = 0;
    player.position.y = 17.5;
    player.position.z = 0;
    scene.add(player);
    return player;
  }

  const player = createPlayer();
  const targetCameraPos = { x: 100, y: 100, z: 100 };

  const cameraFocus = { x: 0, y: 0, z: 0 };
  const targetCameraFocus = { x: 0, y: 0, z: 0 };

  const playerPos = { x: 0, y: 17.5, z: 0 };
  const targetPlayerPos = { x: 0, y: 17.5, z: 0 };

  let speed = 0;
  document.body.addEventListener("click", () => {
    targetCameraPos.z = camera.position.z - 100;

    targetCameraFocus.z -= 100;

    targetPlayerPos.z -= 100;
    speed = 5;
  });

  function moveCamera() {
    const { x, z } = camera.position;
    if (x > targetCameraPos.x) {
      camera.position.x -= 3;
    }
    if (z > targetCameraPos.z) {
      camera.position.z -= 3;
    }

    if (cameraFocus.x > targetCameraFocus.x) {
      cameraFocus.x -= 3;
    }
    if (cameraFocus.z > targetCameraFocus.z) {
      cameraFocus.z -= 3;
    }

    camera.lookAt(cameraFocus.x, cameraFocus.y, cameraFocus.z);
  }

  function movePlayer() {
    if (player.position.x > targetPlayerPos.x) {
      player.position.x -= 3;
    }
    if (player.position.z > targetPlayerPos.z) {
      player.position.z -= 3;
    }
    player.position.y += speed;
    speed -= 0.3;
    if (player.position.y < 17.5) {
      player.position.y = 17.5;
    }
  }

  create();
  render();
});
</script>

<style scoped></style>

```

