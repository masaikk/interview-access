# 计算机视觉

## 基本代码

### mesh模型

#### 展示mesh模型

这里有两份代码，分别是问的chatGPT和new Bing问到的

```python
import openmesh as om
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d.art3d import Poly3DCollection

# 创建一个 TriMesh 对象
mesh = om.TriMesh()

# 添加顶点
v0 = mesh.add_vertex([0, 0, 0])
v1 = mesh.add_vertex([1, 0, 0])
v2 = mesh.add_vertex([1, 1, 0])
v3 = mesh.add_vertex([0, 1, 0])
v4 = mesh.add_vertex([0, 0, 1])
v5 = mesh.add_vertex([1, 0, 1])
v6 = mesh.add_vertex([1, 1, 1])
v7 = mesh.add_vertex([0, 1, 1])

# 添加面片
mesh.add_face(v0, v1, v2)
mesh.add_face(v0, v2, v3)
mesh.add_face(v0, v4, v5)
mesh.add_face(v0, v5, v1)
mesh.add_face(v1, v5, v6)
mesh.add_face(v1, v6, v2)
mesh.add_face(v2, v6, v7)
mesh.add_face(v2, v7, v3)
mesh.add_face(v3, v7, v4)
mesh.add_face(v3, v4, v0)
mesh.add_face(v4, v7, v6)
mesh.add_face(v4, v6, v5)

# 创建一个 Matplotlib 的 3D 坐标轴对象
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

# 遍历面片数据，并绘制每个面片的顶点
for face_handle in mesh.faces():
    vertices = []
    for vh in mesh.fv(face_handle):
        point = mesh.point(vh)
        vertices.append(point)
    vertices = list(zip(*vertices))
    poly = Poly3DCollection([list(zip(vertices[0], vertices[1], vertices[2]))], alpha=0.5)
    ax.add_collection3d(poly)

# 设置坐标轴标签并显示图形
ax.set_xlabel('X')
ax.set_ylabel('Y')
ax.set_zlabel('Z')

plt.show()

```

![image-20230623233543051](CV.assets/image-20230623233543051.png)

```python
import openmesh as om
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

mesh = om.TriMesh()

# 添加顶点
vh0 = mesh.add_vertex([0, 0, 0])
vh1 = mesh.add_vertex([1, 0, 0])
vh2 = mesh.add_vertex([1, 1, 0])
vh3 = mesh.add_vertex([0, 1, 0])
vh4 = mesh.add_vertex([0, 0, 1])
vh5 = mesh.add_vertex([1, 0, 1])
vh6 = mesh.add_vertex([1, 1, 1])
vh7 = mesh.add_vertex([0, 1, 1])

# 添加面
face_vhs = [[vh0,vh1,vh2,vh3],[vh7,vh6,vh5,vh4],[vh4,vh5,vh1,vh0],[vh5,vh6,vh2,vh1],[vh6,vh7,vh3,vh2],[vh7,vh4,vh0,vh3]]
for vhs in face_vhs:
    mesh.add_face(vhs)

vertices = np.zeros((mesh.n_vertices(), 3))
for vh in mesh.vertices():
    vertices[vh.idx()] = mesh.point(vh)

faces = np.zeros((mesh.n_faces(), 3), dtype=np.uint32)
for fh in mesh.faces():
    faces[fh.idx()] = [int(vh.idx()) for vh in mesh.fv(fh)]

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.plot_trisurf(vertices[:,0], vertices[:,1], faces, vertices[:,2], shade=True)
plt.show()

```

![image-20230623233600740](CV.assets/image-20230623233600740.png)

